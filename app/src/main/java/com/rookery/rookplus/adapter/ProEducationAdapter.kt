package com.rookery.rookplus.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.rookery.rookplus.R
import com.rookery.rookplus.model.EducationModel


public class ProEducationAdapter(val educationList: ArrayList<EducationModel>, private val context: Context?) : RecyclerView.Adapter<ProEducationAdapter.ProEducationAdapterItemHolder>() {


    init {
        notifyDataSetChanged()

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProEducationAdapterItemHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.education_item, parent, false)
        val viewHolder = ProEducationAdapterItemHolder(v)
        return viewHolder
    }

    @SuppressLint("ResourceType")
    override fun onBindViewHolder(holder: ProEducationAdapterItemHolder, position: Int) {


        val educationModel = educationList[position]


            holder.txtSchoolName.text = educationModel.sch_name
            holder.txtCompletionYear.text = educationModel.completion
            holder.txtCourse.text = educationModel.course
            holder.txtLevel.text = educationModel.level


    }

    fun alertDialogFrag(req_id: String) {
        val builder = AlertDialog.Builder(context!!)

        val arr = arrayOf("Check Request Details", "Send Message")

        builder.setTitle("GoPharma")
        builder.setItems(arr) { dialog, which ->
            when (which) {
                0 -> {

                    Toast.makeText(context, arr[which], Toast.LENGTH_LONG).show()
                }
                1 -> {
                    Toast.makeText(context, arr[which], Toast.LENGTH_LONG).show()
//                    val intent = Intent(context, PatientChatActivity::class.java)
//                    intent.putExtra("request_id", req_id)
//                    context.startActivity(intent)
                }
                else -> {

                }
            }
        }.show()
    }

    override fun getItemCount(): Int {
        return educationList.size
    }

    class ProEducationAdapterItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtSchoolName: TextView
        var txtCourse: TextView
        var txtCompletionYear: TextView
        var txtLevel: TextView


        init {
            txtSchoolName = itemView.findViewById(R.id.txtSchoolName)
            txtCourse = itemView.findViewById(R.id.txtCourse)
            txtCompletionYear = itemView.findViewById(R.id.txtCompletionYear)
            txtLevel = itemView.findViewById(R.id.txtLevel)
        }
    }
}
