package com.rookery.rookplus.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rookery.rookplus.R
import com.rookery.rookplus.model.CompanyModel


public class CompanyAdapter(val inventoryList: ArrayList<CompanyModel>, private val context: Context?) : RecyclerView.Adapter<CompanyAdapter.CompanyAdapterItemHolder>() {
    //internal var typeface: Typeface


    init {
        notifyDataSetChanged()
        //typeface = Typeface.createFromAsset(context.assets, "Lato-Regular.ttf")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompanyAdapterItemHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.company_item, parent, false)
        val viewHolder = CompanyAdapterItemHolder(v)
        return viewHolder
    }

    @SuppressLint("ResourceType")
    override fun onBindViewHolder(holder: CompanyAdapterItemHolder, position: Int) {


        val companyModel = inventoryList[position]

//        val font_type = Typeface.createFromAsset(context!!.assets, "Raleway-SemiBold.ttf")

//        if (inventory.status == "true") {
            holder.txtCompanyName.text = companyModel.cname
            holder.txtLocation.text = companyModel.location
            holder.txtCompType.text = companyModel.type
            holder.txtCompDesc.text = companyModel.bio


        if (context != null) {
            Glide
                .with(context)
                .load(companyModel.logo)
                .centerCrop()
                .placeholder(R.drawable.image_loading)
                .into(holder.imgCompanyLogo)
        }


//            holder.req_detail.text = inventory.req_detail
//            holder.req_id.text = inventory.request_id
//            if (context != null) {
//                //   holder.status_color.setBackgroundColor(context.resources.getColor(android.R.color.holo_red_light))
//            }
//        } else if (inventory.status == "false") {
//            holder.allergy.text = "${inventory.surname} ${inventory.othernames}"
//            holder.req_detail.text = inventory.req_detail
//            holder.req_id.text = inventory.request_id
//
//            if (context != null) {
//                // holder.status_color.setBackgroundColor(context.resources.getColor(android.R.color.holo_green_light))
//            }
     //   }
        // holder.side_color.text = inventory.request_id

//        holder.cardview.setOnClickListener {
//            alertDialogFrag(inventory.request_id)
//        }

//        holder.req_id.typeface = font_type
//        holder.allergy.typeface = font_type
//        holder.req_detail.typeface = font_type

    }

    fun alertDialogFrag(req_id: String) {
        val builder = AlertDialog.Builder(context!!)

        val arr = arrayOf("Check Request Details", "Send Message")

        builder.setTitle("GoPharma")
        builder.setItems(arr) { dialog, which ->
            when (which) {
                0 -> {

                    Toast.makeText(context, arr[which], Toast.LENGTH_LONG).show()
                }
                1 -> {
                    Toast.makeText(context, arr[which], Toast.LENGTH_LONG).show()
//                    val intent = Intent(context, PatientChatActivity::class.java)
//                    intent.putExtra("request_id", req_id)
//                    context.startActivity(intent)
                }
                else -> {

                }
            }
        }.show()
    }

    override fun getItemCount(): Int {
        return inventoryList.size
    }

    class CompanyAdapterItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtCompanyName: TextView
        var txtLocation: TextView
        var txtCompType: TextView
        var txtCompDesc: TextView
       // var cardview: CardView
        var imgCompanyLogo: ImageView
        var imgMore: ImageView


        init {
            txtCompanyName = itemView.findViewById(R.id.txtCompanyName)
            txtLocation = itemView.findViewById(R.id.txtLocation)
            txtCompType = itemView.findViewById(R.id.txtCompType)
            txtCompDesc = itemView.findViewById(R.id.txtCompDesc)
            imgMore = itemView.findViewById(R.id.imgMoreDetails)
        //    cardview = itemView.findViewById(R.id.cardview1)
            imgCompanyLogo = itemView.findViewById(R.id.imgCompLogo)
            //side_color = itemView.findViewById(R.id.side_color);
        }
    }
}
