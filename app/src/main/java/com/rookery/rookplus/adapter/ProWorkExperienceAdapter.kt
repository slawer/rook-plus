package com.rookery.rookplus.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.rookery.rookplus.R
import com.rookery.rookplus.model.EducationModel
import com.rookery.rookplus.model.WorkExpModel


public class ProWorkExperienceAdapter(val workExpList: ArrayList<WorkExpModel>, private val context: Context?) : RecyclerView.Adapter<ProWorkExperienceAdapter.ProWorkExperienceItemHolder>() {


    init {
        notifyDataSetChanged()

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProWorkExperienceItemHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.work_experience_item, parent, false)
        val viewHolder = ProWorkExperienceItemHolder(v)
        return viewHolder
    }

    @SuppressLint("ResourceType", "SetTextI18n")
    override fun onBindViewHolder(holder: ProWorkExperienceItemHolder, position: Int) {


        val workExpModel = workExpList[position]


            holder.txtWorkPosition.text = workExpModel.title
            holder.txtWorkPeriod.text = "${workExpModel.start_date}-${workExpModel.end_date}"
            holder.txtCompanyName.text = workExpModel.name
            holder.txtLocation.text = workExpModel.location


    }

    fun alertDialogFrag(req_id: String) {
        val builder = AlertDialog.Builder(context!!)

        val arr = arrayOf("Check Request Details", "Send Message")

        builder.setTitle("GoPharma")
        builder.setItems(arr) { dialog, which ->
            when (which) {
                0 -> {

                    Toast.makeText(context, arr[which], Toast.LENGTH_LONG).show()
                }
                1 -> {
                    Toast.makeText(context, arr[which], Toast.LENGTH_LONG).show()
//                    val intent = Intent(context, PatientChatActivity::class.java)
//                    intent.putExtra("request_id", req_id)
//                    context.startActivity(intent)
                }
                else -> {

                }
            }
        }.show()
    }

    override fun getItemCount(): Int {
        return workExpList.size
    }

    class ProWorkExperienceItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtWorkPosition: TextView
        var txtWorkPeriod: TextView
        var txtCompanyName: TextView
        var txtLocation: TextView


        init {
            txtWorkPosition = itemView.findViewById(R.id.txtWorkPosition)
            txtWorkPeriod = itemView.findViewById(R.id.txtWorkPeriod)
            txtCompanyName = itemView.findViewById(R.id.txtCompanyName)
            txtLocation = itemView.findViewById(R.id.txtLocation)
        }
    }
}
