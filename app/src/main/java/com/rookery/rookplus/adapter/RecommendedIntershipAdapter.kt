package com.rookery.rookplus.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rookery.rookplus.R
import com.rookery.rookplus.model.EducationModel
import com.rookery.rookplus.model.RecInternshipModel
import com.rookery.rookplus.model.WorkExpModel
import de.hdodenhof.circleimageview.CircleImageView


public class RecommendedIntershipAdapter(val recInternshipArrayLiat:  ArrayList<RecInternshipModel>, private val context: Context?) : RecyclerView.Adapter<RecommendedIntershipAdapter.RecommendedIntershipAdapterItemHolder>() {


    init {
        notifyDataSetChanged()

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecommendedIntershipAdapterItemHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.rec_internship_item, parent, false)
        val viewHolder = RecommendedIntershipAdapterItemHolder(v)
        return viewHolder
    }

    @SuppressLint("ResourceType", "SetTextI18n")
    override fun onBindViewHolder(holder: RecommendedIntershipAdapterItemHolder, position: Int) {


        val recInternshipModel = recInternshipArrayLiat[position]


            holder.txtIntershipPosition.text = recInternshipModel.title
            holder.txtCompanyName.text = recInternshipModel.cname
            holder.txtLocation.text = recInternshipModel.location

        if (context != null) {
            Glide
                .with(context)
                .load(recInternshipModel.logo)
                .centerCrop()
                .placeholder(R.drawable.image_loading)
                .into(holder.imgLogo)
        }
    }

    fun alertDialogFrag(req_id: String) {
        val builder = AlertDialog.Builder(context!!)

        val arr = arrayOf("Check Request Details", "Send Message")

        builder.setTitle("GoPharma")
        builder.setItems(arr) { dialog, which ->
            when (which) {
                0 -> {

                    Toast.makeText(context, arr[which], Toast.LENGTH_LONG).show()
                }
                1 -> {
                    Toast.makeText(context, arr[which], Toast.LENGTH_LONG).show()
//                    val intent = Intent(context, PatientChatActivity::class.java)
//                    intent.putExtra("request_id", req_id)
//                    context.startActivity(intent)
                }
                else -> {

                }
            }
        }.show()
    }

    override fun getItemCount(): Int {
        return recInternshipArrayLiat.size
    }

    class RecommendedIntershipAdapterItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtIntershipPosition: TextView
        var txtCompanyName: TextView
        var txtLocation: TextView
        var imgLogo: CircleImageView


        init {
            txtIntershipPosition = itemView.findViewById(R.id.txtIntershipPosition)
            txtCompanyName = itemView.findViewById(R.id.txtCompanyName)
            txtLocation = itemView.findViewById(R.id.txtLocation)
            imgLogo = itemView.findViewById(R.id.imgLogo)
        }
    }
}
