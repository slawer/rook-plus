package com.rookery.rookplus.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rookery.rookplus.R
import com.rookery.rookplus.model.*
import de.hdodenhof.circleimageview.CircleImageView


public class EventAdapter(val eventArrayLiat:  ArrayList<EventModel>, private val context: Context?) : RecyclerView.Adapter<EventAdapter.EventAdapterItemHolder>() {


    init {
        notifyDataSetChanged()

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventAdapterItemHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.event_item, parent, false)
        val viewHolder = EventAdapterItemHolder(v)
        return viewHolder
    }

    @SuppressLint("ResourceType", "SetTextI18n")
    override fun onBindViewHolder(holder: EventAdapterItemHolder, position: Int) {


        val eventModel = eventArrayLiat[position]


            holder.txtEventCompany.text = eventModel.cname
            holder.txtPrice.text = "GHs ${eventModel.price}"
            holder.txtEventDetails.text = eventModel.details
            holder.txtEventDate.text = eventModel.event_date
            holder.txtEventLocation.text = eventModel.location
            holder.txtEventTitle.text = eventModel.title

        if (context != null) {
            Glide
                .with(context)
                .load(eventModel.image)
                .centerCrop()
                .placeholder(R.drawable.image_loading)
                .into(holder.imgEventLogo)

            Glide
                .with(context)
                .load(eventModel.logo)
                .centerCrop()
                .placeholder(R.drawable.image_loading)
                .into(holder.imageCLogo)
        }
    }

    fun alertDialogFrag(req_id: String) {
        val builder = AlertDialog.Builder(context!!)

        val arr = arrayOf("Check Request Details", "Send Message")

        builder.setTitle("GoPharma")
        builder.setItems(arr) { dialog, which ->
            when (which) {
                0 -> {

                    Toast.makeText(context, arr[which], Toast.LENGTH_LONG).show()
                }
                1 -> {
                    Toast.makeText(context, arr[which], Toast.LENGTH_LONG).show()
//                    val intent = Intent(context, PatientChatActivity::class.java)
//                    intent.putExtra("request_id", req_id)
//                    context.startActivity(intent)
                }
                else -> {

                }
            }
        }.show()
    }

    override fun getItemCount(): Int {
        return eventArrayLiat.size
    }

    class EventAdapterItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtEventTitle: TextView
        var txtPrice: TextView
        //var txtTimePost: TextView
        var txtEventLocation: TextView
        var txtEventDate: TextView
        var txtEventDetails: TextView
        var txtEventCompany: TextView
        var imageCLogo: ImageView
        var imgEventLogo: ImageView


        init {
            txtEventTitle = itemView.findViewById(R.id.txtEventTitle)
            txtPrice = itemView.findViewById(R.id.txtPrice)
            txtEventLocation = itemView.findViewById(R.id.txtEventLocation)
            txtEventDate = itemView.findViewById(R.id.txtEventDate)
            txtEventDetails = itemView.findViewById(R.id.txtDetails)
            txtEventCompany = itemView.findViewById(R.id.txtEventCompany)
            imageCLogo = itemView.findViewById(R.id.imageCLogo)
            imgEventLogo = itemView.findViewById(R.id.imgEventLogo)
        }
    }
}
