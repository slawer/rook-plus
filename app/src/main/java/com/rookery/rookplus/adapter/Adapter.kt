package com.rookery.rookplus.adapter

import android.content.Context
import androidx.viewpager.widget.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.mikhaellopez.circularimageview.CircularImageView
import com.rookery.rookplus.R
import com.rookery.rookplus.model.Model

class Adapter(private val models: List<Model>, private val context: Context) : PagerAdapter() {
    private var layoutInflater: LayoutInflater? = null

    override fun getCount(): Int {
        return models.size
    }



    override fun isViewFromObject(view: View, o: Any): Boolean {
        return view == o
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = LayoutInflater.from(context)

        val view = layoutInflater!!.inflate(R.layout.onb_item, container, false)

        val imageButton = view.findViewById<CircularImageView>(R.id.imgBbtn)
        val desc = view.findViewById<TextView>(R.id.txtDetails)

        imageButton.setImageResource(models[position].image)
        imageButton.setBackgroundColor(context.resources.getColor(models[position].bg_color))
        desc.text = models[position].desc
        container.addView(view, 0)

        return view
    }



    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}
