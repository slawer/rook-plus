package com.rookery.rookplus.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.rookery.rookplus.R
import com.rookery.rookplus.model.EducationModel
import com.rookery.rookplus.model.PortfolioModel
import com.rookery.rookplus.model.WorkExpModel


public class ProPortfolioAdapter(val portfolioList: ArrayList<PortfolioModel>, private val context: Context?) : RecyclerView.Adapter<ProPortfolioAdapter.ProPortfolioAdapterItemHolder>() {


    init {
        notifyDataSetChanged()

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProPortfolioAdapterItemHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.portfolio_item, parent, false)
        val viewHolder = ProPortfolioAdapterItemHolder(v)
        return viewHolder
    }

    @SuppressLint("ResourceType", "SetTextI18n")
    override fun onBindViewHolder(holder: ProPortfolioAdapterItemHolder, position: Int) {


        val portfolioModel = portfolioList[position]


            holder.txtPortfolioName.text = portfolioModel.title
            holder.txtPortfolioDesc.text = portfolioModel.description
            holder.txtPortfolioPeriod.text = "${portfolioModel.start_date}-${portfolioModel.end_date}"
           // holder.txtLevel.text = portfolioModel.level


    }

    fun alertDialogFrag(req_id: String) {
        val builder = AlertDialog.Builder(context!!)

        val arr = arrayOf("Check Request Details", "Send Message")

        builder.setTitle("GoPharma")
        builder.setItems(arr) { dialog, which ->
            when (which) {
                0 -> {

                    Toast.makeText(context, arr[which], Toast.LENGTH_LONG).show()
                }
                1 -> {
                    Toast.makeText(context, arr[which], Toast.LENGTH_LONG).show()
//                    val intent = Intent(context, PatientChatActivity::class.java)
//                    intent.putExtra("request_id", req_id)
//                    context.startActivity(intent)
                }
                else -> {

                }
            }
        }.show()
    }

    override fun getItemCount(): Int {
        return portfolioList.size
    }

    class ProPortfolioAdapterItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtPortfolioName: TextView
        var txtPortfolioDesc: TextView
        var txtPortfolioPeriod: TextView
       // var txtLevel: TextView


        init {
            txtPortfolioName = itemView.findViewById(R.id.txtPortfolioName)
            txtPortfolioDesc = itemView.findViewById(R.id.txtPortfolioDesc)
            txtPortfolioPeriod = itemView.findViewById(R.id.txtPortPeriod)
           // txtLevel = itemView.findViewById(R.id.txtLevel)
        }
    }
}
