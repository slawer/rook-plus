package com.rookery.rookplus.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rookery.rookplus.R
import com.rookery.rookplus.model.*
import de.hdodenhof.circleimageview.CircleImageView


public class PollAdapter(val pollArrayLiat:  ArrayList<PollModel>, private val context: Context?) : RecyclerView.Adapter<PollAdapter.PollAdapterItemHolder>() {


    init {
        notifyDataSetChanged()

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PollAdapterItemHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.poll_item, parent, false)
        val viewHolder = PollAdapterItemHolder(v)
        return viewHolder
    }

    @SuppressLint("ResourceType", "SetTextI18n")
    override fun onBindViewHolder(holder: PollAdapterItemHolder, position: Int) {


        val pollModel = pollArrayLiat[position]


//            holder.txtIntershipPosition.text = discoverModel.title
//            holder.txtCompanyName.text = discoverModel.cname
            holder.txtPollTitle.text = pollModel.title

        if (context != null) {
            Glide
                .with(context)
                .load(pollModel.logo)
                .centerCrop()
                .placeholder(R.drawable.image_loading)
                .into(holder.imgLogo)
        }
    }

    fun alertDialogFrag(req_id: String) {
        val builder = AlertDialog.Builder(context!!)

        val arr = arrayOf("Check Request Details", "Send Message")

        builder.setTitle("GoPharma")
        builder.setItems(arr) { dialog, which ->
            when (which) {
                0 -> {

                    Toast.makeText(context, arr[which], Toast.LENGTH_LONG).show()
                }
                1 -> {
                    Toast.makeText(context, arr[which], Toast.LENGTH_LONG).show()
//                    val intent = Intent(context, PatientChatActivity::class.java)
//                    intent.putExtra("request_id", req_id)
//                    context.startActivity(intent)
                }
                else -> {

                }
            }
        }.show()
    }

    override fun getItemCount(): Int {
        return pollArrayLiat.size
    }

    class PollAdapterItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//        var txtIntershipPosition: TextView
//        var txtCompanyName: TextView
        var txtPollTitle: TextView
        var imgLogo: ImageView


        init {
//            txtIntershipPosition = itemView.findViewById(R.id.txtIntershipPosition)
//            txtCompanyName = itemView.findViewById(R.id.txtCompanyName)
            txtPollTitle = itemView.findViewById(R.id.txtPollTitle)
            imgLogo = itemView.findViewById(R.id.imgLogo)
        }
    }
}
