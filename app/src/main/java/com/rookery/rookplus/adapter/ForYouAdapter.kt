package com.rookery.rookplus.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rookery.rookplus.R
import com.rookery.rookplus.model.*
import de.hdodenhof.circleimageview.CircleImageView
import androidx.core.content.ContextCompat.startActivity
import android.content.Intent
import android.net.Uri


public class ForYouAdapter(val forYouArrayLiat: ArrayList<ForYouModel>, private val context: Context?) :
    RecyclerView.Adapter<ForYouAdapter.ForYouAdapterItemHolder>() {


    init {
        notifyDataSetChanged()

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForYouAdapterItemHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.for_you_item, parent, false)
        val viewHolder = ForYouAdapterItemHolder(v)
        return viewHolder
    }

    @SuppressLint("ResourceType", "SetTextI18n")
    override fun onBindViewHolder(holder: ForYouAdapterItemHolder, position: Int) {


        val forYouModel = forYouArrayLiat[position]


        holder.txtViews.text = forYouModel.views
        // holder.txtPrice.text = forYouModel.link
        holder.txtTitle.text = forYouModel.title
        holder.txtTimeAgo.text = forYouModel.timepost
        //  holder.txtCName.text = forYouModel.cname
        holder.txtCategory.text = forYouModel.category

        if (context != null) {
            Glide
                .with(context)
                .load(forYouModel.logo)
                .centerCrop()
                .placeholder(R.drawable.image_loading)
                .into(holder.imgLogo)

        }

        holder.imgShare.setOnClickListener {
            val uri = Uri.parse(forYouModel.link)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            context?.startActivity(intent)
        }

    }

    fun alertDialogFrag(req_id: String) {
        val builder = AlertDialog.Builder(context!!)

        val arr = arrayOf("Check Request Details", "Send Message")

        builder.setTitle("GoPharma")
        builder.setItems(arr) { dialog, which ->
            when (which) {
                0 -> {

                    Toast.makeText(context, arr[which], Toast.LENGTH_LONG).show()
                }
                1 -> {
                    Toast.makeText(context, arr[which], Toast.LENGTH_LONG).show()
//                    val intent = Intent(context, PatientChatActivity::class.java)
//                    intent.putExtra("request_id", req_id)
//                    context.startActivity(intent)
                }
                else -> {

                }
            }
        }.show()
    }

    override fun getItemCount(): Int {
        return forYouArrayLiat.size
    }

    class ForYouAdapterItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtViews: TextView
        var txtTitle: TextView
        //var txtTimePost: TextView
        var txtTimeAgo: TextView
        //  var txtCName: TextView
        var txtCategory: TextView
        //        var txtEventCompany: TextView
        var imgLogo: ImageView
        var imgShare: ImageView


        init {
            txtViews = itemView.findViewById(R.id.txtViews)
            txtTitle = itemView.findViewById(R.id.txtTitle)
            txtTimeAgo = itemView.findViewById(R.id.txtTimeAgo)
            //       txtCName = itemView.findViewById(R.id.txtCN)
            txtCategory = itemView.findViewById(R.id.txtCategory)
            imgLogo = itemView.findViewById(R.id.imgLogo)
            imgShare = itemView.findViewById(R.id.imgShare)
        }
    }
}
