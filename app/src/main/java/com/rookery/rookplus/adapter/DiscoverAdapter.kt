package com.rookery.rookplus.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rookery.rookplus.R
import com.rookery.rookplus.model.DiscoverModel
import com.rookery.rookplus.model.EducationModel
import com.rookery.rookplus.model.RecInternshipModel
import com.rookery.rookplus.model.WorkExpModel
import de.hdodenhof.circleimageview.CircleImageView


public class DiscoverAdapter(val discoverArrayLiat:  ArrayList<DiscoverModel>, private val context: Context?) : RecyclerView.Adapter<DiscoverAdapter.DiscoverAdapterItemHolder>() {


    init {
        notifyDataSetChanged()

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiscoverAdapterItemHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.discover_item, parent, false)
        val viewHolder = DiscoverAdapterItemHolder(v)
        return viewHolder
    }

    @SuppressLint("ResourceType", "SetTextI18n")
    override fun onBindViewHolder(holder: DiscoverAdapterItemHolder, position: Int) {


        val discoverModel = discoverArrayLiat[position]


//            holder.txtIntershipPosition.text = discoverModel.title
//            holder.txtCompanyName.text = discoverModel.cname
//            holder.txtLocation.text = discoverModel.location

        if (context != null) {
            Glide
                .with(context)
                .load(discoverModel.image_url)
                .centerCrop()
                .placeholder(R.drawable.image_loading)
                .into(holder.imgDiscover)
        }
    }

    fun alertDialogFrag(req_id: String) {
        val builder = AlertDialog.Builder(context!!)

        val arr = arrayOf("Check Request Details", "Send Message")

        builder.setTitle("GoPharma")
        builder.setItems(arr) { dialog, which ->
            when (which) {
                0 -> {

                    Toast.makeText(context, arr[which], Toast.LENGTH_LONG).show()
                }
                1 -> {
                    Toast.makeText(context, arr[which], Toast.LENGTH_LONG).show()
//                    val intent = Intent(context, PatientChatActivity::class.java)
//                    intent.putExtra("request_id", req_id)
//                    context.startActivity(intent)
                }
                else -> {

                }
            }
        }.show()
    }

    override fun getItemCount(): Int {
        return discoverArrayLiat.size
    }

    class DiscoverAdapterItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//        var txtIntershipPosition: TextView
//        var txtCompanyName: TextView
//        var txtLocation: TextView
        var imgDiscover: ImageView


        init {
//            txtIntershipPosition = itemView.findViewById(R.id.txtIntershipPosition)
//            txtCompanyName = itemView.findViewById(R.id.txtCompanyName)
//            txtLocation = itemView.findViewById(R.id.txtLocation)
            imgDiscover = itemView.findViewById(R.id.imgDiscover)
        }
    }
}
