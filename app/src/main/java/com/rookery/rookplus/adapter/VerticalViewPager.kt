package com.rookery.rookplus.adapter

import android.content.Context
import androidx.viewpager.widget.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

class VerticalViewPager : ViewPager {
    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    private fun init() {
        setPageTransformer(true, VerticalPage())
        overScrollMode = View.OVER_SCROLL_NEVER
    }

    private fun getIntercombinXY(event: MotionEvent): MotionEvent {
        val width = width.toFloat()
        val height = height.toFloat()

        val newX = event.y / height * width
        val newY = event.x / width * height

        event.setLocation(newX, newY)

        return event
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        val intercepted = super.onInterceptTouchEvent(getIntercombinXY(ev))
        getIntercombinXY(ev)

        return intercepted
    }

    override fun onTouchEvent(ev: MotionEvent): Boolean {
        return super.onTouchEvent(getIntercombinXY(ev))
    }



    private inner class VerticalPage : ViewPager.PageTransformer {

        override fun transformPage(view: View, position: Float) {
            if (position < -1) {
                view.alpha = 0f
            } else if (position <= 1) {
                view.alpha = 1f
                view.translationX = view.width * -position
                val yPosition = position * view.height
                view.translationY = yPosition
            } else {
                view.alpha = 0f
            }
        }
    }
}
