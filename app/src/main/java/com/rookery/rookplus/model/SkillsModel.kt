package com.rookery.rookplus.model

data class SkillsModel(
    val title: String, val id: String
)