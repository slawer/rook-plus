package com.rookery.rookplus.model

data class StatsModel(
    val total_tasks: String,
    val completed: String,
    val points: String,
    val success: String,
    val speed: String,
    val badges: String
)