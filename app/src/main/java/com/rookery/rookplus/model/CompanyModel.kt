package com.rookery.rookplus.model

data class CompanyModel(val cname: String, val type: String, val location: String, val bio: String, val logo: String, val subscribed: String)