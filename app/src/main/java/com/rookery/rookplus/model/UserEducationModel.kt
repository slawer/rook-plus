package com.rookery.rookplus.model

data class UserEducationModel(
    val sch_name: String,
    val course: String,
    val completion: String,
    val level: String,
    val id: String
)