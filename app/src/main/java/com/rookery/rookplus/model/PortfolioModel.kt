package com.rookery.rookplus.model

data class PortfolioModel(
    val title: String,
    val id: String,
    val description: String,
    val start_date: String,
    val end_date: String,
    val items: String
)