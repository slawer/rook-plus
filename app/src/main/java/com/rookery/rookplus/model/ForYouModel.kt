package com.rookery.rookplus.model

data class ForYouModel(
    val id: String,
    val views: String,
    val link: String,
    val title: String,
    val timepost: String,
    val cname: String,
    val logo: String,
    val category: String
)