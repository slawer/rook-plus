package com.rookery.rookplus.model

data class EventModel(
    val id: String,
    val image: String,
    val price: String,
    val title: String,
    val timepost: String,
    val location: String,
    val event_date: String,
    val details: String,
    val cname: String,
    val logo: String,
    val category: String
)