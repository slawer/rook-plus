package com.rookery.rookplus.model

data class DemographicsModel(
    val gender: String,
    val nationality: String,
    val city: String,
    val emp_status: String,
    val martital_status: String,
    val birthdate: String,
    val fname: String,
    val lname: String,
    val phone: String,
    val avatar: String,
    val email: String
)