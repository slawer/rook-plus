package com.rookery.rookplus.model

data class DiscoverModel(
    val id: String,
    val image_url: String,
    val type: String,
    val target_id: String
)