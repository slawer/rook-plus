package com.rookery.rookplus.model

data class InterestsModel(val title: String, val id: String)