package com.rookery.rookplus.model

data class RecInternshipModel(
    val title: String,
    val id: String,
    val location: String,
    val type: String,
    val deadline: String,
    val cname: String,
    val logo: String,
    val category: String,
    val is_applied: String,
    val priority: String
)