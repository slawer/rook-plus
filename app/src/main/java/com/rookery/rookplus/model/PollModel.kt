package com.rookery.rookplus.model

data class PollModel(
    val id: String,
    val title: String,
    val options: String,
    val timepost: String,
    val cname: String,
    val logo: String,
    val result: String
)