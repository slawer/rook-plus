package com.rookery.rookplus.model

data class WorkExpModel(
    val name: String,
    val id: String,
    val location: String,
    val start_date: String,
    val end_date: String,
    val title: String,
    val is_current: String
)