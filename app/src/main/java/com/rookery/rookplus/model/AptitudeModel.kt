package com.rookery.rookplus.model

data class AptitudeModel(
    val tests_taken: String,
    val highest_score: String,
    val percentile: String,
    val average_score: String
)