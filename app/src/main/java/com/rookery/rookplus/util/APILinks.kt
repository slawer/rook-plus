package com.rookery.rookplus.util

interface APILinks {

    companion object {
        val API_URL = "https://myrookery.com/api/v0/"

        val EMAIL_REG_URL = "$API_URL/auth/register"
        val SOCIAL_REG_URL = "$API_URL/auth/social_register"
        val EMAIL_LOG_URL = "$API_URL/auth/login"


        val COMP_LIST_URL = "$API_URL/lists/companies"
        val DISCOVER_LIST_URL = "$API_URL/lists/discover"
        val POLL_LIST_URL = "$API_URL/lists/polls"
        val EVENT_LIST_URL = "$API_URL/lists/events"
        val ARTICLE_LIST_URL = "$API_URL/lists/articles"
        val VIDEO_LIST_URL = "$API_URL/lists/videos"
        val INTERNSHIP_LIST_URL = "$API_URL/lists/internships"
        val REC_INTERNSHIP_LIST_URL = "$API_URL/lists/recommended/internships"


        val GET_USER_PRO_URL = "$API_URL/users/profile"
        val UPDATE_USER_DEMO_URL = "$API_URL/users/update_demographics"
        val UPDATE_USER_EDU_URL = "$API_URL/users/update_education"
        val UPDATE_USER_INTEREST_URL = "$API_URL/users/update_interests"
        val UPDATE_USER_PORT_URL = "$API_URL/users/update_portfolio"
        val UPDATE_USER_SKILLS_URL = "$API_URL/users/update_skills"
        val UPDATE_USER_WORK_EXP_URL = "$API_URL/users/update_work_xp"

        val host = "api.linkedin.com"
        val LNK_PRO_URL = "https://$host/v1/people/~:(id,first-name,last-name,email-address)"
    }
}