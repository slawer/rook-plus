package com.rookery.rookplus.activity

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.StrictMode
import android.preference.PreferenceManager
import android.text.Html
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.facebook.FacebookSdk
import com.facebook.GraphResponse
import com.github.kittinunf.fuel.Fuel
import com.linkedin.platform.APIHelper
import com.linkedin.platform.LISession
import com.linkedin.platform.LISessionManager
import com.linkedin.platform.errors.LIApiError
import com.linkedin.platform.errors.LIAuthError
import com.linkedin.platform.listeners.ApiListener
import com.linkedin.platform.listeners.ApiResponse
import com.linkedin.platform.listeners.AuthListener
import com.linkedin.platform.utils.Scope
import com.rookery.rookplus.R
import com.rookery.rookplus.helpers.FacebookHelper
import com.rookery.rookplus.util.APILinks
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText
import com.twitter.sdk.android.core.*
import com.twitter.sdk.android.core.Callback
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import de.hdodenhof.circleimageview.CircleImageView
import dmax.dialog.SpotsDialog
import okhttp3.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.concurrent.TimeUnit

class LoginActivity : AppCompatActivity(),
    View.OnClickListener, FacebookHelper.OnFbSignInListener {

    lateinit var btnSignIn: Button
    lateinit var txtLog: TextView
    lateinit var edtEmail: EditText
    lateinit var edtPassword: ShowHidePasswordEditText

    private lateinit var prefs: SharedPreferences
    private lateinit var edit: SharedPreferences.Editor

    private lateinit var dialog: SpotsDialog

    //--------------------------------Facebook login--------------------------------------//
    private lateinit var fbConnectHelper: FacebookHelper
    private lateinit var fbSignInButton: CircleImageView
    //   private val fbShareButton: Button? = null

    //-----------------------------------Twitter Sign In -----------------------------------//
    // private lateinit var twitterHelper: TwitterHelper
    private lateinit var tSignInButton: CircleImageView

//    private lateinit var mTwitter: TwitterHelper
    //-----------------------------------LinkedIn Sign In-----------------------------------//

    // private val linkedInSignInHelper: FacebookHelper? = null
    private lateinit var lSignInButton: CircleImageView

    private var mTwitterSession: TwitterSession? = null
    private var mTwitterAuthToken: TwitterAuthToken? = null

    lateinit var mTwitterAuthConfig: TwitterAuthConfig

    lateinit var mTwitterAuthClient: TwitterAuthClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mTwitterAuthConfig = TwitterAuthConfig(
            this.resources.getString(R.string.TWITTER_CONSUMER_KEY),
            this.resources.getString(R.string.TWITTER_CONSUMER_SECRET)
        )

        val policy =  StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)


        val config: TwitterConfig = TwitterConfig.Builder(this@LoginActivity)
            .logger(DefaultLogger(Log.DEBUG))
            .twitterAuthConfig(mTwitterAuthConfig)
            .debug(true)
            .build()
        Twitter.initialize(config)

        setContentView(R.layout.activity_login)

        FacebookSdk.sdkInitialize(applicationContext) // Facebook SDK Initialization
        LISessionManager.getInstance(this)
        dialog = SpotsDialog.Builder().setContext(this).build() as SpotsDialog
        prefs = PreferenceManager.getDefaultSharedPreferences(this)
        edit = prefs.edit()


        mTwitterAuthClient = TwitterAuthClient()
        fbConnectHelper = FacebookHelper(this, this)

        btnSignIn = findViewById(R.id.btnSignIn)
        txtLog = findViewById(R.id.txtLog)
        edtEmail = findViewById(R.id.edtEmail)
        edtPassword = findViewById(R.id.edtPassword)

        fbSignInButton = findViewById(R.id.btnRegFacebook)
        tSignInButton = findViewById(R.id.btnRegTwitter)
        lSignInButton = findViewById(R.id.btnRegLinkedIn)



        btnSignIn.setOnClickListener {

            doLogin()
        }


        val first =
            getColoredSpanned("Don't have an account?", resources.getColor(android.R.color.darker_gray).toString())
        val second = getColoredSpanned("<u>Sign Up</u>", resources.getColor(R.color.colorFB).toString())

        txtLog.text = Html.fromHtml("$first $second")

        txtLog.setOnClickListener {
            val intent = Intent(this, SignUpMainActivity::class.java)
            startActivity(intent)
        }


        lSignInButton.setOnClickListener(this)
        fbSignInButton.setOnClickListener(this)
        tSignInButton.setOnClickListener(this)

    }

//    private fun makeRequest() {
//        dialog.setMessage("Loading...")
//        dialog.show()
//        val timeout = 5000 // 5000 milliseconds = 5 seconds.
//        val timeoutRead = 60000 // 60000 milliseconds = 1 minute.
//
//        val email = edtEmail.text.toString().trim()
//        val password = edtPassword.text.toString().trim()
//        val firebaseToken = prefs.getString("firebase_token", "")
//        val type = "email"
//
//        val rootObject = JSONObject()
//        rootObject.put("email", email)
//        rootObject.put("password", password)
//        rootObject.put("firebaseToken", firebaseToken)
//        rootObject.put("type", type)
//
//
//        // FuelManager.instance.basePath =
//        Fuel.post(APILinks.EMAIL_LOG_URL).jsonBody(rootObject.toString()).timeout(timeout).timeoutRead(timeoutRead)
//            .responseString { request, response, result ->
//                //do something with response
//                println(request.httpString())
//                val (data, error) = result
//                if (data != null) {
//                    //do something when success
//
//                    val jO = JSONObject(data)
//                    println(data.toString())
//                    // println(jO)
//                    // println(result.toString())
//
//                    when {
//                        jO.getBoolean("success") == true -> {
//                            val jsonInner = jO.getJSONObject("result")
//
//
//                            val user_jwt = jsonInner.getString("jwt")
//                            val userObj = jsonInner.getJSONObject("user")
//
//                            val user_email = userObj.getString("email")
//                            val user_firebaseToken = userObj.getString("firebaseToken")
//
//                            Log.d("user_jwt", user_jwt)
//                            Log.d("user_email", user_email)
//                            Log.d("user_firebaseToken", user_firebaseToken)
//
//
//                            edit.putString("user_jwt", user_jwt)
//                            edit.putString("user_email", user_email)
//                            edit.putString("user_firebaseToken", user_firebaseToken)
//
//                            edit.commit()
//
//                            val intent = Intent(this, MainHomeActivity::class.java)
//                            startActivity(intent)
//                            finish()
//                            Toast.makeText(this, "Sign In Successful", Toast.LENGTH_LONG).show()
//                            dialog.dismiss()
//                        }
//                        jO.getBoolean("success") == false -> {
//                            Toast.makeText(this, jO.getString("errorMessage"), Toast.LENGTH_LONG).show()
//                            dialog.dismiss()
//                        }
//
//
//
//                    }
//                } else {
//                    //error handling
//                    Toast.makeText(this, "Something went wrong. Please try again later.", Toast.LENGTH_LONG).show()
//                    Log.d("Failed", error.toString())
//                    dialog.dismiss()
//                }
//            }
//    }

    private fun doLogin() {
        val timeout = 5000 // 5000 milliseconds = 5 seconds.
        val timeoutRead = 60000 // 60000 milliseconds = 1 minute.
        dialog.setMessage("Loading...")
        dialog.show()
        val JSON: MediaType = MediaType.get("application/json; charset=utf-8")

        val client = OkHttpClient()
        client.newBuilder().connectTimeout(timeout.toLong(), TimeUnit.MILLISECONDS).readTimeout(timeoutRead.toLong(), TimeUnit.MILLISECONDS)
        //var jbody: ResponseBody? = null
        val email = edtEmail.text.toString().trim()
        val password = edtPassword.text.toString().trim()
        val firebaseToken = prefs.getString("firebase_token", "")
        val type = "email"

        val rootObject = JSONObject()
        rootObject.put("email", email)
        rootObject.put("password", password)
        rootObject.put("firebaseToken", firebaseToken)
        rootObject.put("type", type)
        //rootObject.put("person_type_id", type_id)


        val body = RequestBody.create(JSON, rootObject.toString())
        val request: okhttp3.Request = Request.Builder()
            .url(APILinks.EMAIL_LOG_URL)
            .post(body)
            .build()

        //var response = client.newCall(request).execute()
        var responses: Response? = null

        try {
            responses = client.newCall(request).execute()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        try {
            val jsonData = responses!!.body()?.string()

            Log.d("RESPONSE", jsonData)

            val jO = JSONObject(jsonData)

            when {
                jO.getBoolean("success") -> {
                    val jsonInner = jO.getJSONObject("result")


                    val user_jwt = jsonInner.getString("jwt")
                    val userObj = jsonInner.getJSONObject("user")

                    val user_email = userObj.getString("email")
                    val user_firebaseToken = userObj.getString("firebaseToken")

                    Log.d("user_jwt", user_jwt)
                    Log.d("user_email", user_email)
                    Log.d("user_firebaseToken", user_firebaseToken)


                    edit.putString("user_jwt", user_jwt)
                    edit.putString("user_email", user_email)
                    edit.putString("user_firebaseToken", user_firebaseToken)

                    edit.commit()

                    val intent = Intent(this, MainHomeActivity::class.java)
                    startActivity(intent)
                    finish()
                    Toast.makeText(this, "Sign In Successful", Toast.LENGTH_LONG).show()
                    dialog.dismiss()
                }
                !jO.getBoolean("success") -> {
                    Toast.makeText(this, jO.getString("errorMessage"), Toast.LENGTH_LONG).show()
                    dialog.dismiss()
                }
                else -> {
                    //error handling
                    Toast.makeText(this, "Something went wrong. Please try again later.", Toast.LENGTH_LONG).show()
                    dialog.dismiss()
                }
            }
        } catch (e: Exception) {
            dialog.dismiss()
            Toast.makeText(this,"Something went wrong. Please try again later.", Toast.LENGTH_LONG).show()
        }
    }


//        val Jarray = Jobject.getJSONArray("employees")
//
//        for (i in 0 until Jarray.length()) {
//            val `object` = Jarray.getJSONObject(i)
//        }
//
//
//        client.newCall(request).enqueue(object : Callback {
//            override fun onFailure(call: Call, e: IOException) {
//                dialog.dismiss()
//                Log.d("Error", e.toString())
//            }
//
//            override fun onResponse(call: Call, response: okhttp3.Response) {
//                jbody = response.body()
//
//
//            }
//
//        })
//
//
//        val jO = JSONObject(jbody!!.string())
//
//
//        println(body)
//        println(body2)
//        println("JSONObject $jO")


    private fun doSocialLogin(
        user_token: String,
        social_type: String,
        social_id: String,
        social_email: String
    ) {
        dialog.setMessage("Loading...")
        dialog.show()
        val timeout = 5000 // 5000 milliseconds = 5 seconds.
        val timeoutRead = 60000 // 60000 milliseconds = 1 minute.

//        val email = edtEmail.text.toString().trim()
//        val password = edtPassword.text.toString().trim()
        val firebaseToken = prefs.getString("firebase_token", "")
        val type = "social"

        val rootObject = JSONObject()
        rootObject.put("id", social_id)
        rootObject.put("socialType", social_type)
        rootObject.put("firebaseToken", firebaseToken)
        rootObject.put("type", type)


        // FuelManager.instance.basePath =
        Fuel.post(APILinks.EMAIL_LOG_URL).jsonBody(rootObject.toString()).timeout(timeout).timeoutRead(timeoutRead)
            .responseString { request, response, result ->
                //do something with response
                println(request.httpString())
                val (data, error) = result
                if (data != null) {
                    //do something when success

                    val jO = JSONObject(data)
                    println(data.toString())
                    // println(jO)
                    // println(result.toString())

                    when {
                        jO.getBoolean("success") == true -> {
                            val jsonInner = jO.getJSONObject("result")


                            val user_jwt = jsonInner.getString("jwt")
                            val userObj = jsonInner.getJSONObject("user")

                            val user_email = userObj.getString("email")
                            val user_firebaseToken = userObj.getString("firebaseToken")

                            Log.d("user_jwt", user_jwt)
                            Log.d("user_email", user_email)
                            Log.d("user_firebaseToken", user_firebaseToken)


                            edit.putString("user_jwt", user_jwt)
                            edit.putString("user_email", user_email)
                            edit.putString("user_firebaseToken", user_firebaseToken)

                            edit.commit()

                            val intent = Intent(this, MainHomeActivity::class.java)
                            startActivity(intent)
                            finish()
                            Toast.makeText(this, "Sign In Successful", Toast.LENGTH_LONG).show()
                            dialog.dismiss()
                        }
                        jO.getBoolean("success") == false -> {
                            Toast.makeText(this, jO.getString("errorMessage"), Toast.LENGTH_LONG).show()
                            dialog.dismiss()
                        }


                    }
                } else {
                    //error handling
                    Toast.makeText(this, "Something went wrong. Please try again later.", Toast.LENGTH_LONG).show()
                    Log.d("Failed", error.toString())
                    dialog.dismiss()
                }
            }
    }


    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.btnRegLinkedIn -> {
                    doLinkedInLogin()
                    //    linkedInSignInHelper?.signIn()
                    Toast.makeText(this, "LinkedIn", Toast.LENGTH_LONG).show()
                }

                R.id.btnRegTwitter -> {
                    doTwitterLogin()
                    //            twitterHelper.connect()
                    Toast.makeText(this, "Twitter", Toast.LENGTH_LONG).show()
                }

                R.id.btnRegFacebook -> {
                    fbConnectHelper.connect()
                    Toast.makeText(this, "Facebook", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    fun doTwitterLogin() {

        mTwitterAuthClient.authorize(this@LoginActivity, object : Callback<TwitterSession>() {
            override fun success(result: Result<TwitterSession>) {
                mTwitterSession = TwitterCore.getInstance().sessionManager.activeSession
                mTwitterAuthToken = mTwitterSession!!.authToken
//                userDetails.secret = mTwitterAuthToken!!.token
//                userDetails.token = mTwitterAuthToken!!.secret
//                userDetails.userId = result.data.getUserId()
//                userDetails.userName = result.data.getUserName()

                val user_token = mTwitterAuthToken!!.token
                val user_secret = mTwitterAuthToken!!.secret
                val social_id = result.data.userId.toString()
                //doFinalLogin(user_token, "twitter", social_id)

                Log.e("Twitter Success", result.data.toString())
                Log.e("Twitter UID", result.data.userId.toString())
                Log.e("Twitter AUTHTOKEN", result.data.authToken.toString())
                Log.e("Twitter TOKEN", mTwitterAuthToken!!.token)
                Log.e("Twitter SECRET", mTwitterAuthToken!!.secret)

                Toast.makeText(this@LoginActivity, "Successful", Toast.LENGTH_LONG).show()
                getEmailAddress(user_token, "twitter", social_id)
                // doFinalLogin()
            }

            override fun failure(e: TwitterException) {
                try {
                    mTwitterAuthClient.cancelAuthorize()
                } catch (e: Exception) {

                }
                Log.e("Twitter Error", e.message)
            }
        })
    }

    private fun getEmailAddress(user_token: String, social_type: String, social_id: String) {
        val authClient = TwitterAuthClient()
        authClient.requestEmail(mTwitterSession!!, object : Callback<String>() {
            override fun success(result: Result<String>) {

                Toast.makeText(this@LoginActivity, result.data.toString(), Toast.LENGTH_LONG).show()
                val social_email = result.data.toString()
                doSocialLogin(user_token, "twitter", social_id, social_email)
//                userDetails.userEmail = result.data
//                mOnTwitterSignInListener.OnTwitterSignInComplete(userDetails, null)
            }

            override fun failure(exception: TwitterException) {
                Log.e("Twitter Error", exception.message)
            }
        })
    }

    override fun OnFbSignInComplete(graphResponse: GraphResponse?, error: String?) {
        if (error == null) {
            try {
                val jsonObject = graphResponse!!.jsonObject
                //userName.setText(jsonObject.getString("name"))
                // email.setText(jsonObject.getString("email"))


                val accessToken: String = prefs.getString("accessToken", "")

                val social_id = jsonObject.getString("id")
                val social_email = jsonObject.getString("email")
                val profileImg = "http://graph.facebook.com/$social_id/picture?type=large"


                Log.i("FB SUCCESS", jsonObject.toString())
                Log.i("FB ACCESS", accessToken)

                doSocialLogin(accessToken, "facebook", social_id, social_email)

                Toast.makeText(
                    applicationContext,
                    "Successful",
                    Toast.LENGTH_LONG
                ).show()
            } catch (e: JSONException) {
                Log.i("FB ERROR", e.message)
            }

        }
    }

    private fun doLinkedInLogin() {
        LISessionManager.getInstance(applicationContext).init(this, buildScope(), object : AuthListener {
            override fun onAuthSuccess() {
                val sessionManager: LISessionManager = LISessionManager.getInstance(applicationContext)
                val session: LISession = sessionManager.session

                val accessTokenValid: Boolean = session.isValid

                // setUpdateState()
                if (accessTokenValid) {
                    fetchPersonalInfo(session.toString())
//                    Toast.makeText(
//                        applicationContext,
//                        "success" + LISessionManager.getInstance(applicationContext).session.accessToken.value,
//                        Toast.LENGTH_LONG
//                    ).show()
                } else {
                    Toast.makeText(
                        applicationContext,
                        "Failed To Gain Access",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onAuthError(error: LIAuthError) {
                //   setUpdateState()
                //     (findViewById<View>(R.id.at) as TextView).text = error.toString()
                Toast.makeText(applicationContext, "failed $error", Toast.LENGTH_LONG).show()
                Log.e("AuthError", error.toString())
            }
        }, true)
    }

    private fun buildScope(): Scope {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS)
    }

    private fun doFinalLogin(socialToken: String, socialType: String, socialID: String) {

        val intent = Intent(this, SocialSignUpActivity::class.java)
        startActivity(intent)

        edit.putString("socialToken", socialToken)
        edit.putString("socialType", socialType)
        edit.putString("socialID", socialID)
        edit.commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        //if (fbConnectHelper != null) {
        if (data != null) {
            fbConnectHelper.onActivityResult(requestCode, resultCode, data)
        }
        //   }
//        if (twitterHelper != null) {
        if (data != null) {
            // var mTwitterAuthClient: TwitterAuthClient  = TwitterAuthClient()
            mTwitterAuthClient.onActivityResult(requestCode, resultCode, data)
        }
//        }
//        if (linkedInSignInHelper != null) {
        if (data != null) {
            //  linkedInSignInHelper.onActivityResult(requestCode, resultCode, data)
            // linkedInSignInHelper.onActivityResult(requestCode, resultCode, data)
            LISessionManager.getInstance(applicationContext).onActivityResult(this, requestCode, resultCode, data)
        }
        //    }
//        if (isFbLogin) {
//            progressBar.setVisibility(View.VISIBLE);
//            isFbLogin = false;
//        }
    }

    private fun fetchPersonalInfo(session_token: String) {
        val apiHelper = APIHelper.getInstance(applicationContext)
        apiHelper.getRequest(this, APILinks.LNK_PRO_URL, object : ApiListener {
            override fun onApiSuccess(result: ApiResponse) {
                val jsonObject = result.responseDataAsJson

                val social_id = jsonObject.getString("id")
                val first_name = jsonObject.getString("firstName")
                val last_name = jsonObject.getString("lastName")
                val social_email = jsonObject.getString("emailAddress")

                Log.d("LINKEDIN JSON", jsonObject.toString())
                doSocialLogin(session_token, "linkedin", social_id, social_email)
                Toast.makeText(this@LoginActivity, "Successful", Toast.LENGTH_LONG).show()
                // Toast.makeText(this@SignUpMainActivity, "$first_name $last_name", Toast.LENGTH_LONG).show()
            }

            override fun onApiError(error: LIApiError) {
                Log.e("LINKEDIN ERROR", error.toString())
                Log.e("LINKEDIN ERROR MSG", error.message)
            }
        })
    }

    private fun getColoredSpanned(text: String, color: String): String {
        return "<font color=$color>$text</font>"
    }
}

