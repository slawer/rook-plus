package com.rookery.rookplus.activity

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import com.github.kittinunf.fuel.Fuel
import com.google.android.material.button.MaterialButton
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.rookery.rookplus.R
import com.rookery.rookplus.util.APILinks
import dmax.dialog.SpotsDialog
import org.json.JSONArray
import org.json.JSONObject

class UserInterestsUpdateActivity : AppCompatActivity(), View.OnClickListener {


    lateinit var chip_group_interests: ChipGroup

    lateinit var btnAddTags: ImageButton
    lateinit var btnSaveChanges: MaterialButton

    lateinit var edtTags: EditText

    private lateinit var dialog: SpotsDialog

    private lateinit var prefs: SharedPreferences
    private lateinit var edit: SharedPreferences.Editor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_interests_update)

        dialog = SpotsDialog.Builder().setContext(this).build() as SpotsDialog

        prefs = PreferenceManager.getDefaultSharedPreferences(this)
        edit = prefs.edit()

        btnAddTags = findViewById(R.id.btnAddTag)
        btnSaveChanges = findViewById(R.id.btnSaveChanges)

        edtTags = findViewById(R.id.edtTags)


        val user_interest_str = prefs.getString("user_interests", "")

        chip_group_interests = findViewById(R.id.chip_group_interests)

        btnAddTags.setOnClickListener(this)
        btnSaveChanges.setOnClickListener(this)

        postUserInterest(user_interest_str)
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when(v.id){
                R.id.btnAddTag ->{
                    if(edtTags.text.toString().isNotEmpty()) {
                        addUserInterest()
                    }else{
                        Toast.makeText(this,"Add an interest",Toast.LENGTH_LONG).show()
                    }


                }

                R.id.btnSaveChanges ->{
                    splittingWords()
                }
            }
        }
    }


    fun postUserInterest(user_interests: String) {
        val _interests =
            user_interests.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        val inflater = LayoutInflater.from(this)
        for (text in _interests) {
            val chip_item = inflater.inflate(R.layout.user_update_interests_chip_item, null, false) as Chip
            chip_item.text = text
//                chip_item.setOnClickListener {view->
//                    chip_group.removeView(view)
//                }
            chip_group_interests.addView(chip_item)
        }
    }

    fun addUserInterest() {
        val _interests =
            edtTags.text.toString().split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        val inflater = LayoutInflater.from(this)
        for (text in _interests) {
            val chip_item = inflater.inflate(R.layout.user_update_interests_chip_item, null, false) as Chip
            chip_item.text = text
//                chip_item.setOnClickListener {view->
//                    chip_group.removeView(view)
//                }
            edtTags.setText("")
            chip_group_interests.addView(chip_item)
        }
    }

    fun splittingWords(){
        val jAr = JSONArray()

        var result = StringBuilder("")
        for (i in 0 until chip_group_interests.childCount) {
            val chip = chip_group_interests.getChildAt(i) as Chip
            // if (chip.isChecked) {
            result.append(chip.text).append(" ")
            //  }
        }

        val _interests =
            result.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        for (text in _interests) {
            println("Values $text")
            val jOb = JSONObject()
            jAr.put(jOb.put("title",text))

            println("JSON Object $jOb")
            println("JSON Array Inside $jAr")

        }


        println("JSON Array Outside $jAr")


        updateInterests(jAr)

    }

    private fun updateInterests(jsonInterest: JSONArray){
        dialog.setMessage("Loading...")
        dialog.show()
        val timeout = 5000 // 5000 milliseconds = 5 seconds.
        val timeoutRead = 60000 // 60000 milliseconds = 1 minute.


        val rootObject = JSONObject()
        rootObject.put("interests", jsonInterest)
        rootObject.put("count", jsonInterest.length() )


        println("Request Body $rootObject")

        // FuelManager.instance.basePath =
        Fuel.post(APILinks.UPDATE_USER_INTEREST_URL).header("x-ROOK-token" to prefs.getString("user_jwt", "")).jsonBody(rootObject.toString()).timeout(timeout).timeoutRead(timeoutRead).responseString { request, response, result ->
            //do something with response
            println(request.httpString())
            val (data, error) = result
            if (error == null) {
                //do something when success

                val jO = JSONObject(data)
                println(data.toString())
                // println(jO)
                // println(result.toString())

                when {
                    jO.getBoolean("success") -> {


                        finish()
                        Toast.makeText(this,"Update Successful",Toast.LENGTH_LONG).show()

                        dialog.dismiss()
                    }
                    !jO.getBoolean("success") -> {
                        Toast.makeText(this, jO.getString("errorMessage"), Toast.LENGTH_LONG).show()
                        dialog.dismiss()
                    }

                    else -> {
                        Toast.makeText(this, "Something went wrong. Please try again later.", Toast.LENGTH_LONG).show()
                        Log.d("Failed", jO.toString())
                        dialog.dismiss()
                    }

                }
            } else {
                //error handling
                Toast.makeText(this, "Something went wrong. Please try again later.", Toast.LENGTH_LONG).show()
                Log.d("Failed", error.toString())
                dialog.dismiss()
            }
        }
    }
}
