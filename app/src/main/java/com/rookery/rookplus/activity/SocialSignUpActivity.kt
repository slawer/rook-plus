package com.rookery.rookplus.activity

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.hbb20.CountryCodePicker
import com.rookery.rookplus.R

class SocialSignUpActivity : AppCompatActivity() {

    lateinit var ccp: CountryCodePicker
    lateinit var editTextCarrierNumber: EditText
    lateinit var btnNext: Button
    lateinit var edtFname: EditText
    lateinit var edtLname: EditText

    private lateinit var prefs: SharedPreferences
    private lateinit var edit: SharedPreferences.Editor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_social_sign_up)

        prefs = PreferenceManager.getDefaultSharedPreferences(this)
        edit = prefs.edit()

        ccp = findViewById(R.id.ccp)
        editTextCarrierNumber = findViewById(R.id.editText_carrierNumber)
        edtLname = findViewById(R.id.edtLname)
        edtFname = findViewById(R.id.edtFname)

        ccp.registerCarrierNumberEditText(editTextCarrierNumber)
        ccp.setPhoneNumberValidityChangeListener { isValidNumber ->

            //            if(isValidNumber){
//                Toast.makeText(this, "Valid".trim(), Toast.LENGTH_LONG).show()
//            }else{
//                Toast.makeText(this, "Not Valid", Toast.LENGTH_LONG).show()
//            }

        }

        btnNext = findViewById(R.id.btnNext)

        btnNext.setOnClickListener {

            doNext()
        }

        ccp.isMotionEventSplittingEnabled = false
    }

    private fun doNext() {

        if (editTextCarrierNumber.text.toString().isEmpty()) {
            editTextCarrierNumber.error = "Enter a phone number"

        } else if (edtFname.text.toString().isEmpty()) {
            edtFname.error = "Please enter first name"
        } else if (edtLname.text.toString().isEmpty()) {
            edtLname.error = "Please enter last name"
        } else {

            var phone_number = editTextCarrierNumber.text.toString()
            //val new_phone_number = editTextCarrierNumber.text.toString()

            if (editTextCarrierNumber.text.toString().contains(" ")) {
                phone_number = phone_number.replace(" ", "")
            }

            edit.putString("country_code", ccp.selectedCountryCode.toString())
            edit.putString("phone_number", phone_number)
            edit.putString("full_phone_number", ccp.fullNumber)
            edit.putString("first_name", edtFname.text.toString())
            edit.putString("last_name", edtLname.text.toString())
            edit.commit()
            val intent = Intent(this, SocialSignUpVerificationActivity::class.java)
            startActivity(intent)
        }
    }
}
