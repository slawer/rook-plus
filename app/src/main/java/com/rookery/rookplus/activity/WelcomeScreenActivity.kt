package com.rookery.rookplus.activity

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.PagerAdapter
import androidx.appcompat.app.AppCompatActivity
import com.rookery.rookplus.R
import com.rookery.rookplus.adapter.VPagerAdapter
import com.rookery.rookplus.adapter.VerticalViewPager
import com.rookery.rookplus.fragment.*

class WelcomeScreenActivity : AppCompatActivity() {

    lateinit var viewPager: VerticalViewPager
    lateinit var pagerAdapter: PagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_screen)

        val list: ArrayList<Fragment> = java.util.ArrayList()
        list.add(WelcomeFragment())
        list.add(EngageFeedFragment())
        list.add(BuildProfileFragment())
        list.add(CompleteTaskFragment())
        list.add(InternshipsFragment())

        viewPager = findViewById(R.id.pager)
        pagerAdapter = VPagerAdapter(supportFragmentManager,list)

        viewPager.adapter = pagerAdapter
    }
}
