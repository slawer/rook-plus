package com.rookery.rookplus.activity

import android.animation.ArgbEvaluator
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Base64
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.rookery.rookplus.R
import com.rookery.rookplus.adapter.Adapter
import com.rookery.rookplus.model.Model
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class MainActivity : AppCompatActivity(), ViewPager.OnPageChangeListener {


    private lateinit var viewPager: ViewPager
    lateinit var adap: Adapter
    lateinit var modelsList: ArrayList<Model>

    // private var mDots: Array<Any>? = null
    private var mDotsLayout: LinearLayout? = null

    private lateinit var prefs: SharedPreferences
    private lateinit var edit: SharedPreferences.Editor


    lateinit var btnNext: Button
    var colors = ArrayList<Int>()
    var argbEvaluator = ArgbEvaluator()

    val mDots = arrayOfNulls<ImageView>(3)
    private var dotscount: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        prefs = PreferenceManager.getDefaultSharedPreferences(this@MainActivity)
        edit = prefs.edit()

        btnNext = findViewById(R.id.btnBegin)
        mDotsLayout = findViewById(R.id.dotsLayout)

        generateFBToken()
        printKeyHash()

        modelsList = java.util.ArrayList()

        modelsList.add(
            Model(
                R.drawable.cvonboard,
                R.color.colorFB,
                "With fully established CV, get recommended to companies for internships."
            )
        )
        modelsList.add(
            Model(
                R.drawable.jobonboard,
                R.color.colorDarkOrange,
                "Gain work experience virtually at your own time and effort."
            )
        )
        modelsList.add(Model(R.drawable.xpoboard, R.color.colorSkyBlueDark, "Become job ready by learning from experience."))

        adap = Adapter(modelsList, this)

        viewPager = findViewById(R.id.viewPager)
        viewPager.adapter = adap

        dotscount = adap.count

        val colors_tmp = arrayListOf(
            resources.getColor(R.color.colorBlue),
            resources.getColor(R.color.colorOrange),
            resources.getColor(R.color.colorSkyBlue)
        )
        addDotsIndicator(0)
        colors.addAll(colors_tmp)

        viewPager.setOnPageChangeListener(this)
        btnNext.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)

        }

    }

    private fun generateFBToken(){
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("FB_ERROR", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token

                // Log and toast
                val msg = getString(R.string.msg_token_fmt, token)

                edit.putString("firebase_token", token)
                edit.commit()

                Log.d("FBT_SUCCESS", msg)
                //Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
            })
        // [END retrieve_current_token]
    }

    private fun printKeyHash() {
        try {
            val info = packageManager.getPackageInfo(
                "com.rookery.rookplus",
                PackageManager.GET_SIGNATURES
            )

            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KEYHASH", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }

        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }

    }

    private fun addDotsIndicator(position: Int) {

        // mDots = arrayOf<Array>(4)
        mDotsLayout!!.removeAllViews()
        for (i in mDots.indices) {
            mDots[i] = ImageView(this)

            mDots[i]!!.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.inactive_dot))
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )

            params.setMargins(12, 0, 8, 0)
            mDotsLayout!!.addView(mDots[i], params)
        }

        if (mDots.isNotEmpty()) {
            mDots[position]!!.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.active_dot))
        }
    }

    override fun onPageScrollStateChanged(position: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
//        addDotsIndicator(position)

        if (position < (adap.count - 1) && position < (colors.size - 1)) {
            viewPager.setBackgroundColor(
                argbEvaluator.evaluate(
                    positionOffset,
                    colors[position],
                    colors[position + 1]
                ) as Int
            )
        } else {
            viewPager.setBackgroundColor(colors[colors.size - 1])
        }



    }

    override fun onPageSelected(position: Int) {
        for(i in 0 until dotscount){
            mDots[i]!!.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.inactive_dot))
        }

        mDots[position]!!.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.active_dot))

    }

}
