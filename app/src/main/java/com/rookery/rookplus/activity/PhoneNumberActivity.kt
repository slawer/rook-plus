package com.rookery.rookplus.activity

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.appcompat.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.hbb20.CountryCodePicker
import com.rookery.rookplus.R


class PhoneNumberActivity : AppCompatActivity(){

    lateinit var btnNext: Button
    lateinit var ccp: CountryCodePicker
    lateinit var editTextCarrierNumber: EditText


    private lateinit var prefs: SharedPreferences
    private lateinit var edit: SharedPreferences.Editor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_phone_number)
       // setContentView(MainLayout(this, null))

        prefs = PreferenceManager.getDefaultSharedPreferences(this)
        edit = prefs.edit()

        ccp =  findViewById(R.id.ccp)
        editTextCarrierNumber =  findViewById(R.id.editText_carrierNumber)

        ccp.registerCarrierNumberEditText(editTextCarrierNumber)
        ccp.setPhoneNumberValidityChangeListener { isValidNumber ->

//            if(isValidNumber){
//                Toast.makeText(this, "Valid".trim(), Toast.LENGTH_LONG).show()
//            }else{
//                Toast.makeText(this, "Not Valid", Toast.LENGTH_LONG).show()
//            }

        }

        ccp.isMotionEventSplittingEnabled =false

        btnNext = findViewById(R.id.btnNext)

        btnNext.setOnClickListener {

//            val intent = Intent(this, SignUpVerificationActivity::class.java)
//            startActivity(intent)
            doNext()
          //   Toast.makeText(this, editTextCarrierNumber.text.toString().trim(), Toast.LENGTH_LONG).show()
        }
    }


    private fun doNext() {

        if (editTextCarrierNumber.text.toString().isEmpty()) {
            editTextCarrierNumber.error = "Enter a phone number"

        } else {

            var phone_number = editTextCarrierNumber.text.toString()
            //val new_phone_number = editTextCarrierNumber.text.toString()

            if(editTextCarrierNumber.text.toString().contains(" ")){
                phone_number = phone_number.replace(" ","")
            }

            edit.putString("country_code", ccp.selectedCountryCode.toString())
            edit.putString("phone_number",phone_number)
            edit.putString("full_phone_number", ccp.fullNumber)
            edit.commit()
            val intent = Intent(this, SignUpVerificationActivity::class.java)
            startActivity(intent)
        }
    }


}
