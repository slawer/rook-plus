package com.rookery.rookplus.activity

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.Html
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.facebook.FacebookSdk
import com.facebook.GraphResponse
import com.linkedin.platform.APIHelper
import com.linkedin.platform.LISession
import com.linkedin.platform.LISessionManager
import com.linkedin.platform.errors.LIApiError
import com.linkedin.platform.errors.LIAuthError
import com.linkedin.platform.listeners.ApiListener
import com.linkedin.platform.listeners.ApiResponse
import com.linkedin.platform.listeners.AuthListener
import com.linkedin.platform.utils.Scope
import com.rookery.rookplus.R
import com.rookery.rookplus.helpers.FacebookHelper
import com.rookery.rookplus.util.APILinks.Companion.LNK_PRO_URL
import com.twitter.sdk.android.core.*
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import org.json.JSONException
import java.util.*


class SignUpMainActivity : AppCompatActivity(),
    View.OnClickListener, FacebookHelper.OnFbSignInListener {


    lateinit var txtTnC: TextView
    lateinit var btnRegEmail: Button

    private val TAG = MainActivity::class.java.simpleName


    //--------------------------------Facebook login--------------------------------------//
    private lateinit var fbConnectHelper: FacebookHelper
    private lateinit var fbSignInButton: Button
    //   private val fbShareButton: Button? = null

    //-----------------------------------Twitter Sign In -----------------------------------//
    // private lateinit var twitterHelper: TwitterHelper
    private lateinit var tSignInButton: Button

//    private lateinit var mTwitter: TwitterHelper
    //-----------------------------------LinkedIn Sign In-----------------------------------//

    // private val linkedInSignInHelper: FacebookHelper? = null
    private lateinit var lSignInButton: Button

    private var mTwitterSession: TwitterSession? = null
    private var mTwitterAuthToken: TwitterAuthToken? = null

    lateinit var mTwitterAuthConfig: TwitterAuthConfig

    lateinit var mTwitterAuthClient: TwitterAuthClient

    private lateinit var prefs: SharedPreferences
    private lateinit var edit: SharedPreferences.Editor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mTwitterAuthConfig = TwitterAuthConfig(
            this.resources.getString(R.string.TWITTER_CONSUMER_KEY),
            this.resources.getString(R.string.TWITTER_CONSUMER_SECRET)
        )

        val config: TwitterConfig = TwitterConfig.Builder(this@SignUpMainActivity)
            .logger(DefaultLogger(Log.DEBUG))
            .twitterAuthConfig(mTwitterAuthConfig)
            .debug(true)
            .build()
        Twitter.initialize(config)

        setContentView(R.layout.activity_sign_up_main)


        FacebookSdk.sdkInitialize(applicationContext) // Facebook SDK Initialization


        prefs = PreferenceManager.getDefaultSharedPreferences(this)
        edit = prefs.edit()


        mTwitterAuthClient = TwitterAuthClient()
        fbConnectHelper = FacebookHelper(this, this)

        txtTnC = findViewById(R.id.txtTnC)
        btnRegEmail = findViewById(R.id.btnRegEmail)
        fbSignInButton = findViewById(R.id.btnRegFacebook)
        tSignInButton = findViewById(R.id.btnRegTwitter)
        lSignInButton = findViewById(R.id.btnRegLinkedIn)

        val name = getColoredSpanned(
            "By registering with us, you are accepting our",
            resources.getColor(android.R.color.black).toString()
        )
        val surName = getColoredSpanned("<u>terms and conditions</u>", resources.getColor(R.color.colorFB).toString())

        txtTnC.text = Html.fromHtml("$name $surName")

        btnRegEmail.setOnClickListener {
            val intent = Intent(this, SignUpNEActivity::class.java)
            startActivity(intent)
        }

        lSignInButton.setOnClickListener(this)
        fbSignInButton.setOnClickListener(this)
        tSignInButton.setOnClickListener(this)


    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.btnRegLinkedIn -> {
                    doLinkedInLogin()
                    //    linkedInSignInHelper?.signIn()
                    Toast.makeText(this, "LinkedIn", Toast.LENGTH_LONG).show()
                }

                R.id.btnRegTwitter -> {
                    doTwitterLogin()
                    //            twitterHelper.connect()
                    Toast.makeText(this, "Twitter", Toast.LENGTH_LONG).show()
                }

                R.id.btnRegFacebook -> {
                    fbConnectHelper.connect()
                    Toast.makeText(this, "Facebook", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    fun onTwitterSignIn(authToken: String?, secret: String?, userId: Long) {
        Toast.makeText(
            this, String.format(
                Locale.US, "User id:%d\n\nAuthToken:%s\n\nAuthSecret:%s", userId, authToken,
                secret
            ), Toast.LENGTH_LONG
        ).show()
    }

    fun onTwitterError(p0: String?) {

    }

    fun doTwitterLogin() {

        mTwitterAuthClient.authorize(this@SignUpMainActivity, object : Callback<TwitterSession>() {
            override fun success(result: Result<TwitterSession>) {
                mTwitterSession = TwitterCore.getInstance().sessionManager.activeSession
                mTwitterAuthToken = mTwitterSession!!.authToken
//                userDetails.secret = mTwitterAuthToken!!.token
//                userDetails.token = mTwitterAuthToken!!.secret
//                userDetails.userId = result.data.getUserId()
//                userDetails.userName = result.data.getUserName()

                val user_token = mTwitterAuthToken!!.token
                val user_secret = mTwitterAuthToken!!.secret
                val social_id = result.data.userId.toString()

                Log.e("Twitter Success", result.data.toString())
                Log.e("Twitter UID", result.data.userId.toString())
                Log.e("Twitter AUTHTOKEN", result.data.authToken.toString())
                Log.e("Twitter TOKEN", mTwitterAuthToken!!.token)
                Log.e("Twitter SECRET", mTwitterAuthToken!!.secret)

                Toast.makeText(this@SignUpMainActivity, "Successful", Toast.LENGTH_LONG).show()
                getEmailAddress(user_token, "twitter", social_id)
                // doFinalLogin()
            }

            override fun failure(e: TwitterException) {
                try {
                    mTwitterAuthClient.cancelAuthorize()
                } catch (e: Exception) {

                }
                Log.e("Twitter Error", e.message)
            }
        })
    }

    private fun getEmailAddress(user_token: String, social_type: String, social_id: String) {
        val authClient = TwitterAuthClient()
        authClient.requestEmail(mTwitterSession!!, object : Callback<String>() {
            override fun success(result: Result<String>) {

                Toast.makeText(this@SignUpMainActivity, result.data.toString(), Toast.LENGTH_LONG).show()
                Log.d("Twitter Email", result.data.toString())
                val user_email = result.data.toString()
                doFinalLogin(user_token, social_type, social_id, user_email)
//                userDetails.userEmail = result.data
//                mOnTwitterSignInListener.OnTwitterSignInComplete(userDetails, null)
            }

            override fun failure(exception: TwitterException) {
                Log.e("Twitter Error", exception.message)
            }
        })
    }

    override fun OnFbSignInComplete(graphResponse: GraphResponse?, error: String?) {
        if (error == null) {
            try {
                val jsonObject = graphResponse!!.jsonObject
                //userName.setText(jsonObject.getString("name"))
                // email.setText(jsonObject.getString("email"))


                val accessToken: String = prefs.getString("accessToken", "")

                val social_id = jsonObject.getString("id")
                val social_email = jsonObject.getString("email")
                val profileImg = "http://graph.facebook.com/$social_id/picture?type=large"


                Log.i("FB SUCCESS", jsonObject.toString())
                Log.i("FB ACCESS", accessToken)

                doFinalLogin(accessToken, "facebook", social_id, social_email)

                Toast.makeText(
                    applicationContext,
                    "Successful",
                    Toast.LENGTH_LONG
                ).show()
            } catch (e: JSONException) {
                Log.i("FB ERROR", e.message)
            }

        }
    }

    private fun doLinkedInLogin() {

        if (!LISessionManager.getInstance(this).session.isValid) {

            LISessionManager.getInstance(applicationContext).init(this, buildScope(), object : AuthListener {
                override fun onAuthSuccess() {
//                    val sessionManager: LISessionManager = LISessionManager.getInstance(applicationContext)
//                    val session: LISession = sessionManager.session
//
//                    val accessTokenValid: Boolean = session.isValid

                    // setUpdateState()
//                    if (accessTokenValid) {
                        fetchPersonalInfo()
//                    Toast.makeText(
//                        applicationContext,
//                        "success" + LISessionManager.getInstance(applicationContext).session.accessToken.value,
//                        Toast.LENGTH_LONG
//                    ).show()
//                    } else {
                        Toast.makeText(
                            applicationContext,
                            "Failed To Gain Access",
                            Toast.LENGTH_LONG
                        ).show()
//                    }
                }

                override fun onAuthError(error: LIAuthError) {
                    //   setUpdateState()
                    //     (findViewById<View>(R.id.at) as TextView).text = error.toString()
                    Toast.makeText(applicationContext, "failed $error", Toast.LENGTH_LONG).show()
                    Log.e("AuthError", error.toString())
                }
            }, true)

        }
        else{
            Toast.makeText(this, "You are already authenticated.", Toast.LENGTH_SHORT).show();

            //if user is already authenticated fetch basic profile data for user
            fetchPersonalInfo()
        }
    }

    private fun buildScope(): Scope {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS, Scope.W_SHARE)
    }

    private fun doFinalLogin(
        socialToken: String,
        socialType: String,
        socialID: String,
        user_email: String
    ) {

        val intent = Intent(this, SocialSignUpActivity::class.java)
        startActivity(intent)

        edit.putString("socialToken", socialToken)
        edit.putString("socialType", socialType)
        edit.putString("socialID", socialID)
        edit.putString("socialEmail", user_email)
        edit.commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        //if (fbConnectHelper != null) {
        if (data != null) {
            fbConnectHelper.onActivityResult(requestCode, resultCode, data)
        }
        //   }
//        if (twitterHelper != null) {
        if (data != null) {
            // var mTwitterAuthClient: TwitterAuthClient  = TwitterAuthClient()
            mTwitterAuthClient.onActivityResult(requestCode, resultCode, data)
        }
//        }
//        if (linkedInSignInHelper != null) {
        if (data != null) {
            //  linkedInSignInHelper.onActivityResult(requestCode, resultCode, data)
            // linkedInSignInHelper.onActivityResult(requestCode, resultCode, data)
            LISessionManager.getInstance(applicationContext).onActivityResult(this, requestCode, resultCode, data)
        }
        //    }
//        if (isFbLogin) {
//            progressBar.setVisibility(View.VISIBLE);
//            isFbLogin = false;
//        }
    }

    private fun fetchPersonalInfo() {

         val url = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,headline,public-profile-url,picture-url,email-address,picture-urls::(original))";


        val apiHelper = APIHelper.getInstance(applicationContext)
        apiHelper.getRequest(this, url, object : ApiListener {
            override fun onApiSuccess(result: ApiResponse) {
                val jsonObject = result.responseDataAsJson

                val social_id = jsonObject.getString("id")
                val first_name = jsonObject.getString("firstName")
                val last_name = jsonObject.getString("lastName")
                val email_address = jsonObject.getString("emailAddress")

                Log.d("LINKEDIN JSON", jsonObject.toString())
                //doFinalLogin(session_token, "linkedin", social_id, email_address)
                Toast.makeText(this@SignUpMainActivity, "Successful", Toast.LENGTH_LONG).show()
                // Toast.makeText(this@SignUpMainActivity, "$first_name $last_name", Toast.LENGTH_LONG).show()
            }

            override fun onApiError(error: LIApiError) {
                Log.e("LINKEDIN ERROR", error.toString())
                Log.e("LINKEDIN ERROR MSG", error.message)
            }
        })
    }

    private fun getColoredSpanned(text: String, color: String): String {
        return "<font color=$color>$text</font>"
    }
}
