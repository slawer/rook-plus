package com.rookery.rookplus.activity

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import com.github.kittinunf.fuel.Fuel
import com.mukesh.OnOtpCompletionListener
import com.mukesh.OtpView
import com.rookery.rookplus.R
import com.rookery.rookplus.util.APILinks
import dmax.dialog.SpotsDialog
import org.json.JSONObject

class SocialSignUpVerificationActivity : AppCompatActivity(), OnOtpCompletionListener {


    private lateinit var txtVerifMsg: TextView
    private lateinit var otpView: OtpView

    private lateinit var prefs: SharedPreferences
    private lateinit var edit: SharedPreferences.Editor

    private lateinit var dialog: SpotsDialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_social_sign_up_verification)

        dialog = SpotsDialog.Builder().setContext(this).build() as SpotsDialog
        otpView = findViewById(R.id.otp_view)

        txtVerifMsg=findViewById(R.id.txtVerifMsg)

        prefs = PreferenceManager.getDefaultSharedPreferences(this)
        edit = prefs.edit()

        otpView.setOtpCompletionListener(this)
        val displayNumber = prefs.getString("phone_number", "")
        val countryCode = prefs.getString("country_code", "")

        try {
            val intDisNumber = displayNumber.toInt()

            txtVerifMsg.append("+"+formatNumberAsCode(intDisNumber.toString(),countryCode))
            Toast.makeText(this, formatNumberAsCode(intDisNumber.toString(),countryCode), Toast.LENGTH_LONG).show()
        } catch (e: NumberFormatException) {
        }
    }

    private fun formatNumberAsCode(s: String, countryCode: String?): String {

        val oldString = s.substring(0, 5)
        val lastString = s.substring(6, 9)
        val maskString = oldString.replace(oldString,"******")
        val newString = "$countryCode $maskString $lastString"


        return newString
        //return String.format("%s-%s-%s", s.substring(0, 3), s.substring(3, 6), s.substring(6))
    }

    override fun onOtpCompleted(otp: String?) {
        Toast.makeText(this, "OTP $otp", Toast.LENGTH_SHORT).show()
        makeRequest()
    }

    private fun makeRequest(){
        dialog.setMessage("Loading...")
        dialog.show()
        val timeout = 5000 // 5000 milliseconds = 5 seconds.
        val timeoutRead = 60000 // 60000 milliseconds = 1 minute.

        val email = prefs.getString("socialEmail", "")
        val socialID = prefs.getString("socialID", "")
        val firebaseToken = prefs.getString("firebase_token", "")
        val firstname = prefs.getString("first_name", "")
        val lastname = prefs.getString("last_name", "")
        val socialToken = prefs.getString("socialToken", "")
        val socialType = prefs.getString("socialType", "")
        val phone = prefs.getString("full_phone_number", "")

        val rootObject = JSONObject()
        rootObject.put("email", email)
        rootObject.put("socialID", socialID)
        rootObject.put("firebaseToken", firebaseToken)
        rootObject.put("firstname", firstname)
        rootObject.put("lastname", lastname)
        rootObject.put("socialToken", socialToken)
        rootObject.put("socialType", socialType)
        rootObject.put("phone", phone)

        println("Social Json Obeject $rootObject")
        Log.d("Social Json",rootObject.toString())

        // FuelManager.instance.basePath =
        Fuel.post(APILinks.SOCIAL_REG_URL).jsonBody(rootObject.toString()).timeout(timeout).timeoutRead(timeoutRead).responseString { request, response, result ->
            //do something with response
            println(request.httpString())
            val (data, error) = result
            if (error == null) {
                //do something when success

                val jO = JSONObject(data)
                println(data.toString())
                // println(jO)
                // println(result.toString())

                when {
                    jO.getBoolean("success") -> {
                        val jsonInner = jO.getJSONObject("result")


                        val user_jwt = jsonInner.getString("jwt")
                        val userObj = jsonInner.getJSONObject("user")
                        val user_email = userObj.getString("email")
                        val user_firstname = userObj.getString("firstname")
                        val user_lastname = userObj.getString("lastname")
                        val user_firebaseToken = userObj.getString("firebaseToken")

                        Log.d("user_jwt", user_jwt)
                        Log.d("user_email", user_email)
                        Log.d("user_firstname", user_firstname)
                        Log.d("user_lastname", user_lastname)
                        Log.d("user_firebaseToken", user_firebaseToken)


                        edit.putString("user_jwt", user_jwt)
                        edit.putString("user_email", user_email)
                        edit.putString("user_firstname", user_firstname)
                        edit.putString("user_lastname", user_lastname)
                        edit.putString("user_firebaseToken", user_firebaseToken)

                        edit.commit()

                        val intent = Intent(this,LoginActivity::class.java)
                        startActivity(intent)
                        finish()
                        Toast.makeText(this,"Sign Up Successful",Toast.LENGTH_LONG).show()

                        dialog.dismiss()
                    }
                    !jO.getBoolean("success") -> {
                        Toast.makeText(this, jO.getString("errorMessage"), Toast.LENGTH_LONG).show()
                        dialog.dismiss()
                    }

                    else -> {
                        Toast.makeText(this, "Something went wrong. Please try again later.", Toast.LENGTH_LONG).show()
                        Log.d("Failed", jO.toString())
                        dialog.dismiss()
                    }

                }
            } else {
                //error handling
                Toast.makeText(this, "Something went wrong. Please try again later.", Toast.LENGTH_LONG).show()
                Log.d("Failed", error.toString())
                dialog.dismiss()
            }
        }
    }
}
