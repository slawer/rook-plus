package com.rookery.rookplus.activity

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.rookery.rookplus.R

class SignUpNEActivity : AppCompatActivity() {

    lateinit var btnNext: Button
    lateinit var edtFname: EditText
    lateinit var edtLname: EditText
    lateinit var edtEmail: EditText
    private lateinit var prefs: SharedPreferences
    private lateinit var edit: SharedPreferences.Editor
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_ne)


        prefs = PreferenceManager.getDefaultSharedPreferences(this)
        edit = prefs.edit()

        edtLname = findViewById(R.id.edtLname)
        edtFname = findViewById(R.id.edtFname)
        edtEmail = findViewById(R.id.edtEmail)

        btnNext = findViewById(R.id.btnNext)

        btnNext.setOnClickListener {

            doNext()
        }
    }

    private fun doNext() {

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(edtEmail.text!!.toString()).matches() && edtEmail.text.toString().isEmpty()) run {
            edtEmail.error = "Please enter a valid email"
        }
        else if (edtFname.text.toString().isEmpty()) {
            edtFname.error = "Please enter first name"
        } else if (edtLname.text.toString().isEmpty()) {
            edtLname.error = "Please enter last name"
        } else {


            edit.putString("first_name", edtFname.text.toString())
            edit.putString("last_name", edtLname.text.toString())
            edit.putString("email", edtEmail.text.toString())
            edit.commit()
            val intent = Intent(this, SignUpDOBActivity::class.java)
            startActivity(intent)
        }
    }
}
