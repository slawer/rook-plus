package com.rookery.rookplus.activity

import android.app.DatePickerDialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.rookery.rookplus.R
import java.text.SimpleDateFormat
import java.util.*


class SignUpDOBActivity : AppCompatActivity() {


    lateinit var btnNext: Button
    lateinit var imgMale: ImageView
    lateinit var imgFemale: ImageView
    lateinit var edtDateofBirth: EditText

    private lateinit var prefs: SharedPreferences
    private lateinit var edit: SharedPreferences.Editor

    val TAG = "MainDOB"
    lateinit var onDateSetListener: DatePickerDialog.OnDateSetListener

    var gender = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_dob)

        prefs = PreferenceManager.getDefaultSharedPreferences(this)
        edit = prefs.edit()

        btnNext = findViewById(R.id.btnNext)
        imgMale = findViewById(R.id.imgMale)
        imgFemale = findViewById(R.id.imgFemale)
        edtDateofBirth = findViewById(R.id.edtDateofBirth)

        onDateSetListener = DatePickerDialog.OnDateSetListener{ datePicker, year, month, day ->
           // val month = month + 1
          //  Log.d(TAG, "onDateSet: dd/mm/yyyy$day/$month/$year")
            var dateSting: String
            var month: Int = month + 1
            var withZero: String
            if (month < 10) {
                withZero = String.format("%02d", month)
                dateSting = "$day/$withZero/$year"
            } else {
                dateSting = "$day/$month/$year"
            }

            println(dateSting)

            val df = SimpleDateFormat("dd/MM/yyyy")
            val date = df.parse(dateSting)
            val df1 = SimpleDateFormat("dd MMMM yyyy")
            System.out.println(df1.format(date))
            edtDateofBirth.setText(df1.format(date))
            //val date = "$day/" + month + "/" + year
            Log.d(TAG, "onDateSet: ${df1.format(date)}")
           // edtDob.setText(date)
        }


        btnNext.setOnClickListener {

            doNext()
            //Toast.makeText(this, "$gender\n${edtDateofBirth.text}\nComing Soon", Toast.LENGTH_LONG).show()
        }


        imgFemale.setOnClickListener {
            grayScalingFemaleActive()
        }

        imgMale.setOnClickListener {
            grayScalingMaleActive()
        }

        edtDateofBirth.setOnClickListener {
            //getDateOfBirth()
            getDOB()
        }

        grayScaling()


    }

    fun getDOB() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        val dialog = DatePickerDialog(
            this@SignUpDOBActivity,
            android.R.style.Theme_Holo_Light_Dialog_MinWidth,
            onDateSetListener,
            year,
            month,
            day
        )
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.datePicker.maxDate = System.currentTimeMillis()
        dialog.show()
    }

    private fun doNext() {

        if (edtDateofBirth.text.toString().isEmpty() || gender.isEmpty()) {
            edtDateofBirth.error = "Select a valid date"
            Toast.makeText(this,"Select a gender",Toast.LENGTH_LONG).show()

        } else {


            edit.putString("date_of_birth", edtDateofBirth.text.toString())
            edit.putString("gender", gender)
            edit.commit()
            val intent = Intent(this, SignUpPasswordActivity::class.java)
            startActivity(intent)
        }
    }

    private fun getDateOfBirth() {
        //Calendar
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->

            //            val c = Calendar.getInstance()
//            val currentDate = DateFormat.getDateInstance(DateFormat.FULL).format(c.time)
            //   val month = month
            var dateSting: String
            var month: Int = month + 1
            var withZero: String
            if (month < 10) {
                withZero = String.format("%02d", month)
                dateSting = "$dayOfMonth/$withZero/$year"
            } else {
                dateSting = "$dayOfMonth/$month/$year"
            }

            println(dateSting)

            val df = SimpleDateFormat("dd/MM/yyyy")
            val date = df.parse(dateSting)
            val df1 = SimpleDateFormat("dd MMMM yyyy")
            System.out.println(df1.format(date))

            val userAge = GregorianCalendar(year, month, day)
            val minAdultAge = GregorianCalendar()
            //           minAdultAge.add(Calendar.YEAR, -18)
//            if (minAdultAge.before(userAge)) {
//                Toast.makeText(
//                    this,
//                    "You're not allowed to use this service. Only persons above 18+.",
//                    Toast.LENGTH_LONG
//                ).show()
//            } else {
            edtDateofBirth.setText(df1.format(date))
            // edtDateofBirth.setText(dateSting)
            //   }
        }, year, month, day)
        dpd.datePicker.maxDate = System.currentTimeMillis()
        dpd.show()


    }

    private fun grayScaling() {
        val matrix = ColorMatrix()
        matrix.setSaturation(0f)
        val filter = ColorMatrixColorFilter(matrix)
        imgMale.colorFilter = filter
        imgFemale.colorFilter = filter

        gender = ""

    }

    private fun grayScalingFemaleActive() {
        val matrix = ColorMatrix()
        val matrix1 = ColorMatrix()
        matrix.setSaturation(0f)
        matrix1.setSaturation(1f)
        val filter = ColorMatrixColorFilter(matrix)
        val filter1 = ColorMatrixColorFilter(matrix1)
        imgMale.colorFilter = filter
        imgFemale.colorFilter = filter1

        gender = "f"
    }

    private fun grayScalingMaleActive() {
        val matrix = ColorMatrix()
        val matrix1 = ColorMatrix()
        matrix.setSaturation(0f)
        matrix1.setSaturation(1f)
        val filter = ColorMatrixColorFilter(matrix)
        val filter1 = ColorMatrixColorFilter(matrix1)
        imgMale.colorFilter = filter1
        imgFemale.colorFilter = filter


        gender = "m"
    }

}
