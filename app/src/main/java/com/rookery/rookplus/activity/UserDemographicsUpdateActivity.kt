package com.rookery.rookplus.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.rookery.rookplus.R

class UserDemographicsUpdateActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demographics_update)
    }
}
