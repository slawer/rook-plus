package com.rookery.rookplus.activity

import android.content.Intent
import android.os.Bundle
import com.google.android.material.chip.Chip
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import com.rookery.rookplus.R
import kotlinx.android.synthetic.main.activity_interests.*

class InterestsActivity : AppCompatActivity() {

    lateinit var chip: Chip
    lateinit var btnAddTag: ImageButton
    lateinit var btnGetStarted: Button
    lateinit var edtTags: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_interests)
        chip = findViewById(R.id.chip)
        btnAddTag = findViewById(R.id.btnAddTag)
        edtTags = findViewById(R.id.edtTags)
        btnGetStarted = findViewById(R.id.btnGetStarted)

        postDefaultInterest()

        btnAddTag.setOnClickListener {
            val text_array =
                edtTags.text.toString().split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val inflater = LayoutInflater.from(this)
            for (text in text_array) {
                val chip_item = inflater.inflate(R.layout.chip_item, null, false) as Chip
                chip_item.text = text
//                chip_item.setOnClickListener {view->
//                    chip_group.removeView(view)
//                }
                chip_group.addView(chip_item)
            }
        }

        btnGetStarted.setOnClickListener {
            val intent = Intent(this,WelcomeScreenActivity::class.java)
            startActivity(intent)
//            var result = StringBuilder("")
//            for (i in 0 until chip_group.childCount) {
//                val chip = chip_group.getChildAt(i) as Chip
//                if (chip.isChecked) {
//                    result.append(chip.text).append(",")
//                }
//            }
//            Toast.makeText(this, result.toString(), Toast.LENGTH_LONG).show()
        }
    }

    fun postDefaultInterest() {
        val default_interest =
            "php music dance singing".split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        val inflater = LayoutInflater.from(this)
        for (text in default_interest) {
            val chip_item = inflater.inflate(R.layout.chip_item, null, false) as Chip
            chip_item.text = text
//                chip_item.setOnClickListener {view->
//                    chip_group.removeView(view)
//                }
            chip_group.addView(chip_item)
        }
    }
}
