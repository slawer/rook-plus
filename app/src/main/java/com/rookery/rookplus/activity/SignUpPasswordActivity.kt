package com.rookery.rookplus.activity

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.rookery.rookplus.R
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText


class SignUpPasswordActivity : AppCompatActivity() {


    lateinit var edtPassword: ShowHidePasswordEditText

    var str: TextView? = null
    var i = 1
    var pb: ProgressBar? = null

    var stateOkay: Boolean = false
    var stateStrong: Boolean = false
    var stateCap: Boolean = false
    var stateNum: Boolean = false
    var stateSpecial: Boolean = false

    lateinit var imageCharLength: ImageView
    lateinit var imageSpecialChar: ImageView
    lateinit var imageCapitalLetter: ImageView

    lateinit var txtStrength: TextView

    lateinit var btnNext: Button

    private lateinit var prefs: SharedPreferences
    private lateinit var edit: SharedPreferences.Editor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_password)

        prefs = PreferenceManager.getDefaultSharedPreferences(this)
        edit = prefs.edit()

        edtPassword = findViewById(R.id.edtPassword)
        str = findViewById(R.id.textView1)
        pb = findViewById(R.id.progressBar)

        imageCapitalLetter = findViewById(R.id.imageCapitalLetter)
        imageCharLength = findViewById(R.id.imageCharLength)
        imageSpecialChar = findViewById(R.id.imageSpecialChar)

        txtStrength = findViewById(R.id.txtStrength)

        btnNext = findViewById(R.id.btnNext)

        btnNext.setOnClickListener {
            doNext()
        }


        edtPassword.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
                // TODO Auto-generated method stub
                if (edtPassword.text.isEmpty()) {
                    edtPassword.error = "Enter your password..!"
                } else {
                    caculation()
                }

            }

            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
                // TODO Auto-generated method stub

            }

        })

        // caculation()
    }

    private fun doNext() {

        if (edtPassword.text.toString().isEmpty()) {
            edtPassword.error = "Enter a valid password"

        } else {

            if (stateOkay || stateStrong && (stateCap && stateNum && stateSpecial)) {
                edit.putString("password", edtPassword.text.toString())
                edit.commit()
                val intent = Intent(this, PhoneNumberActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(this, "Check your password", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun caculation() {
        // TODO Auto-generated method stub
        val temp = edtPassword.text.toString()
        println("$i current password is : $temp")
        i += 1

        var length = 0
        var uppercase = 0
        var lowercase = 0
        var digits = 0
        var symbols = 0
        var bonus = 0
        var requirements = 0

        var lettersonly = 0
        var numbersonly = 0
        var cuc = 0
        var clc = 0

        length = temp.length
        for (i in 0 until temp.length) {
            when {
                Character.isUpperCase(temp[i]) -> uppercase++
                Character.isLowerCase(temp[i]) -> lowercase++
                Character.isDigit(temp[i]) -> digits++
            }

            symbols = length - uppercase - lowercase - digits

        }

        for (j in 1 until temp.length - 1) {

            if (Character.isDigit(temp[j]))
                bonus++

        }

        var k = 0
        while (k < temp.length) {

            if (Character.isUpperCase(temp[k])) {
                k++

                if (k < temp.length) {

                    if (Character.isUpperCase(temp.get(k))) {

                        cuc++
                        k--

                    }

                }

            }
            k++

        }

        var l = 0
        while (l < temp.length) {

            if (Character.isLowerCase(temp.get(l))) {
                l++

                if (l < temp.length) {

                    if (Character.isLowerCase(temp.get(l))) {

                        clc++
                        l--

                    }

                }

            }
            l++

        }

        println("length$length")
        println("uppercase$uppercase")
        println("lowercase$lowercase")
        println("digits$digits")
        println("symbols$symbols")
        println("bonus$bonus")
        println("cuc$cuc")
        println("clc$clc")

        if (length > 7) {
            requirements++
            imageCharLength.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_check_green))
            stateNum = true
        } else if (length <= 7) {
            stateNum = false
            imageCharLength.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_check_white))
        }

        if (uppercase > 0) {
            requirements++
            stateCap = true
            imageCapitalLetter.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_check_green))
        } else if (uppercase <= 0) {
            stateCap = false
            imageCapitalLetter.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_check_white))
        }

        if (lowercase > 0) {
            requirements++
        }

        if (digits > 0) {
            requirements++
        }

        if (symbols > 0) {
            stateSpecial = true
            requirements++
            imageSpecialChar.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_check_green))
        } else if (symbols <= 0) {
            stateSpecial = false
            imageSpecialChar.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_check_white))
        }

        if (bonus > 0) {
            requirements++
        }

        if (digits == 0 && symbols == 0) {
            lettersonly = 1
        }

        if (lowercase == 0 && uppercase == 0 && symbols == 0) {
            numbersonly = 1
        }

        val Total = ((length * 4 + (length - uppercase) * 2
                + (length - lowercase) * 2 + digits * 4 + symbols * 6
                + bonus * 2 + requirements * 2) - lettersonly * length * 2
                - numbersonly * length * 3 - cuc * 2 - clc * 2)

        println("Total$Total")

        when {
            Total < 30 -> {
                pb!!.progress = Total - 15
                txtStrength.text = getString(R.string.very_weak)
                txtStrength.setTextColor(resources.getColor(android.R.color.holo_red_dark))
                Log.d("strength", "VERY WEAK")
            }
            Total in 40..49 -> {
                txtStrength.text = getString(R.string.weak)
                txtStrength.setTextColor(resources.getColor(android.R.color.holo_orange_light))
                pb!!.progress = Total - 20
                Log.d("strength", "WEAK")
            }
            Total in 56..69 -> {
                stateOkay = true
                stateStrong = false
                txtStrength.text = getString(R.string.okay)
                txtStrength.setTextColor(resources.getColor(android.R.color.white))
                pb!!.progress = Total - 25
                Log.d("strength", "Okay")
            }
            Total >= 76 -> {

                stateOkay = false
                stateStrong = true

                txtStrength.text = getString(R.string.strong)
                txtStrength.setTextColor(resources.getColor(R.color.colorGreen))
                Log.d("strength", "STRONG")
                pb!!.progress = Total - 30
            }
            else -> pb!!.progress = Total - 20
        }

    }
}
