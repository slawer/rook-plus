package com.rookery.rookplus.activity

import android.annotation.SuppressLint
import android.app.Service
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelManager
import com.mukesh.OnOtpCompletionListener
import com.mukesh.OtpView
import com.rookery.rookplus.R
import com.rookery.rookplus.util.APILinks
import dmax.dialog.SpotsDialog
import org.json.JSONObject


class SignUpVerificationActivity : AppCompatActivity(), View.OnFocusChangeListener, View.OnKeyListener, TextWatcher,
    OnOtpCompletionListener {


    private lateinit var mPinHiddenEditText: EditText
    private lateinit var pinProceedButton: Button
    private lateinit var txtVerifMsg: TextView
    private lateinit var otpView: OtpView

    private lateinit var prefs: SharedPreferences
    private lateinit var edit: SharedPreferences.Editor

    private lateinit var dialog: SpotsDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_verification)

        dialog = SpotsDialog.Builder().setContext(this).build() as SpotsDialog

        txtVerifMsg=findViewById(R.id.txtVerifMsg)

        prefs = PreferenceManager.getDefaultSharedPreferences(this)
        edit = prefs.edit()

        init()
        setPINListeners()
        pinProceedButton.setOnClickListener { v ->
            //    Log.d("PIN", mPinHiddenEditText.text.toString())
//            val intent = Intent(this, InterestsActivity::class.java)
//            startActivity(intent)
        }
//        btnClear.setOnClickListener {
//            val intent = Intent(this,InterestsActivity::class.java)
//            startActivity(intent)
//        }

        val displayNumber = prefs.getString("phone_number", "")
        val countryCode = prefs.getString("country_code", "")

        try {
            val intDisNumber = displayNumber.toInt()

            txtVerifMsg.append("+"+formatNumberAsCode(intDisNumber.toString(),countryCode))
            Toast.makeText(this, formatNumberAsCode(intDisNumber.toString(),countryCode), Toast.LENGTH_LONG).show()
        } catch (e: NumberFormatException) {
        }


    }

    fun hideSoftKeyboard(editText: EditText?) {
        if (editText == null)
            return

        val imm = getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(editText.windowToken, 0)
    }


    private fun init() {
//        mPinFirstDigitEditText = findViewById(R.id.pin_first_edittext)
//        mPinSecondDigitEditText = findViewById(R.id.pin_second_edittext)
//        mPinThirdDigitEditText = findViewById(R.id.pin_third_edittext)
//        mPinForthDigitEditText = findViewById(R.id.pin_forth_edittext)
        mPinHiddenEditText = findViewById(R.id.pin_hidden_edittext)
        otpView = findViewById(R.id.otp_view)
        pinProceedButton = findViewById(R.id.btnProceed)
        // btnClear = findViewById(R.id.btnClear)
    }


    private fun formatNumberAsCode(s: String, countryCode: String?): String {

        val oldString = s.substring(0, 5)
        val lastString = s.substring(6, 9)
        val maskString = oldString.replace(oldString,"******")
        val newString = "$countryCode $maskString $lastString"


        return newString
        //return String.format("%s-%s-%s", s.substring(0, 3), s.substring(3, 6), s.substring(6))
    }


    private fun makeRequest(){
        dialog.setMessage("Loading...")
        dialog.show()
        val timeout = 5000 // 5000 milliseconds = 5 seconds.
        val timeoutRead = 60000 // 60000 milliseconds = 1 minute.

        val email = prefs.getString("email", "")
        val password = prefs.getString("password", "")
        val firebaseToken = prefs.getString("firebase_token", "")
        val firstname = prefs.getString("first_name", "")
        val lastname = prefs.getString("last_name", "")
        val gender = prefs.getString("gender", "")
        val dateOfBirth = prefs.getString("date_of_birth", "")
        val phone = prefs.getString("full_phone_number", "")

        val rootObject = JSONObject()
        rootObject.put("email", email)
        rootObject.put("password", password )
        rootObject.put("firebaseToken", firebaseToken)
        rootObject.put("firstname", firstname)
        rootObject.put("lastname", lastname)
        rootObject.put("gender", gender)
        rootObject.put("dateOfBirth", dateOfBirth)
        rootObject.put("phone", phone)

       // FuelManager.instance.basePath =
        Fuel.post(APILinks.EMAIL_REG_URL).jsonBody(rootObject.toString()).timeout(timeout).timeoutRead(timeoutRead).responseString { request, response, result ->
            //do something with response
            println(request.httpString())
            val (data, error) = result
            if (error == null) {
                //do something when success

                val jO = JSONObject(data)
                println(data.toString())
                // println(jO)
                // println(result.toString())

                when {
                    jO.getBoolean("success") -> {
                        val jsonInner = jO.getJSONObject("result")


                        val user_jwt = jsonInner.getString("jwt")
                        val userObj = jsonInner.getJSONObject("user")
                        val user_email = userObj.getString("email")
                        val user_firstname = userObj.getString("firstname")
                        val user_lastname = userObj.getString("lastname")
                        val user_firebaseToken = userObj.getString("firebaseToken")

                        Log.d("user_jwt", user_jwt)
                        Log.d("user_email", user_email)
                        Log.d("user_firstname", user_firstname)
                        Log.d("user_lastname", user_lastname)
                        Log.d("user_firebaseToken", user_firebaseToken)


                        edit.putString("user_jwt", user_jwt)
                        edit.putString("user_email", user_email)
                        edit.putString("user_firstname", user_firstname)
                        edit.putString("user_lastname", user_lastname)
                        edit.putString("user_firebaseToken", user_firebaseToken)

                        edit.commit()

                        val intent = Intent(this,LoginActivity::class.java)
                        startActivity(intent)
                        finish()
                        Toast.makeText(this,"Sign Up Successful",Toast.LENGTH_LONG).show()

                        dialog.dismiss()
                    }
                    !jO.getBoolean("success") -> {
                        Toast.makeText(this, jO.getString("errorMessage"), Toast.LENGTH_LONG).show()
                        dialog.dismiss()
                    }

                    else -> {
                        Toast.makeText(this, "Something went wrong. Please try again later.", Toast.LENGTH_LONG).show()
                        Log.d("Failed", jO.toString())
                        dialog.dismiss()
                    }

                }
            } else {
                //error handling
                Toast.makeText(this, "Something went wrong. Please try again later.", Toast.LENGTH_LONG).show()
                Log.d("Failed", error.toString())
                dialog.dismiss()
            }
        }
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    @SuppressLint("SetTextI18n")
    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//        setDefaultPinBackground(mPinFirstDigitEditText)
//        setDefaultPinBackground(mPinSecondDigitEditText)
//        setDefaultPinBackground(mPinThirdDigitEditText)
//        setDefaultPinBackground(mPinForthDigitEditText)
//        //setDefaultPinBackground(mPinFifthDigitEditText);
//
//        when {
//            s.isEmpty() -> {
//                setFocusedPinBackground(mPinFirstDigitEditText)
//                mPinFirstDigitEditText.setText("")
//            }
//            s.length == 1 -> {
//                setFocusedPinBackground(mPinSecondDigitEditText)
//                mPinFirstDigitEditText.setText(s[0] + "")
//                mPinSecondDigitEditText.setText("")
//                mPinThirdDigitEditText.setText("")
//                mPinForthDigitEditText.setText("")
//                //mPinFifthDigitEditText.setText("");
//            }
//            s.length == 2 -> {
//                setFocusedPinBackground(mPinThirdDigitEditText)
//                mPinSecondDigitEditText.setText(s[1] + "")
//                mPinThirdDigitEditText.setText("")
//                mPinForthDigitEditText.setText("")
//                //mPinFifthDigitEditText.setText("");
//            }
//            s.length == 3 -> {
//                setFocusedPinBackground(mPinForthDigitEditText)
//                mPinThirdDigitEditText.setText(s[2] + "")
//                mPinForthDigitEditText.setText("")
//                //mPinFifthDigitEditText.setText("");
//            }
//            s.length == 4 -> //setFocusedPinBackground(mPinFifthDigitEditText);
//                mPinForthDigitEditText.setText(s[3] + "")
//            //mPinFifthDigitEditText.setText("");
//        }/* else if (s.length() == 5) {
//                setDefaultPinBackground(mPinFifthDigitEditText);
//                mPinFifthDigitEditText.setText(s.charAt(4) + "");
//
//                hideSoftKeyboard(mPinFifthDigitEditText);
//            }*/
//        /* else if (s.length() == 5) {
//                   setDefaultPinBackground(mPinFifthDigitEditText);
//                   mPinFifthDigitEditText.setText(s.charAt(4) + "");
//
//                   hideSoftKeyboard(mPinFifthDigitEditText);
//               }*/
    }

    override fun afterTextChanged(s: Editable) {

    }

    override fun onFocusChange(v: View, hasFocus: Boolean) {
        val id = v.id
        when (id) {
//            R.id.pin_first_edittext -> if (hasFocus) {
//                setFocus(mPinHiddenEditText)
//                showSoftKeyboard(mPinHiddenEditText)
//            }
//
//            R.id.pin_second_edittext -> if (hasFocus) {
//                setFocus(mPinHiddenEditText)
//                showSoftKeyboard(mPinHiddenEditText)
//            }
//
//            R.id.pin_third_edittext -> if (hasFocus) {
//                setFocus(mPinHiddenEditText)
//                showSoftKeyboard(mPinHiddenEditText)
//            }
//
//            R.id.pin_forth_edittext -> if (hasFocus) {
//                setFocus(mPinHiddenEditText)
//                showSoftKeyboard(mPinHiddenEditText)
//            }
//            else -> {
//            }
        }
    }

    override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            val id = v.id
//            when (id) {
//                R.id.pin_hidden_edittext -> if (keyCode == KeyEvent.KEYCODE_DEL) {
//                    //Log.d("PIN", mPinHiddenEditText.getText().subSequence(0, mPinHiddenEditText.length() - 1) + "");
//                    when {
//                        mPinHiddenEditText.text.length == 4 -> mPinForthDigitEditText.setText("")
//                        mPinHiddenEditText.text.length == 3 -> mPinThirdDigitEditText.setText("")
//                        mPinHiddenEditText.text.length == 2 -> mPinSecondDigitEditText.setText("")
//                        mPinHiddenEditText.text.length == 1 -> mPinFirstDigitEditText.setText("")
//                    }
//
//                    if (mPinHiddenEditText.length() > 0)
//                    //Log.d("PIN", mPinHiddenEditText.getText().subSequence(0, mPinHiddenEditText.length() - 1) + "");
//                        mPinHiddenEditText.setText(
//                            mPinHiddenEditText.text.subSequence(
//                                0,
//                                mPinHiddenEditText.length() - 1
//                            )
//                        )
//                    return true
//                }
//                else -> return false
//            }
        }

        return false
    }

    override fun onPointerCaptureChanged(hasCapture: Boolean) {

    }


    /**
     * Sets default PIN background.
     *
     * @param editText edit text to change
     */
    private fun setDefaultPinBackground(editText: EditText) {
        //setViewBackground(editText, getResources().getDrawable(R.drawable.textfield_default_holo_light));
    }

    fun setFocus(editText: EditText?) {
        if (editText == null)
            return

        editText.isFocusable = true
        editText.isFocusableInTouchMode = true
        editText.requestFocus()
    }

    /**
     * Sets focused PIN field background.
     *
     * @param editText edit text to change
     */
    private fun setFocusedPinBackground(editText: EditText) {
        //setViewBackground(editText, getResources().getDrawable(R.drawable.textfield_focused_holo_light));
    }


    private fun setPINListeners() {
        otpView.setOtpCompletionListener(this)
        mPinHiddenEditText.addTextChangedListener(this)
//        mPinFirstDigitEditText.onFocusChangeListener = this
//        mPinSecondDigitEditText.onFocusChangeListener = this
//        mPinThirdDigitEditText.onFocusChangeListener = this
//        mPinForthDigitEditText.onFocusChangeListener = this
//        mPinFirstDigitEditText.setOnKeyListener(this)
//        mPinSecondDigitEditText.setOnKeyListener(this)
//        mPinThirdDigitEditText.setOnKeyListener(this)
//        mPinForthDigitEditText.setOnKeyListener(this)
        mPinHiddenEditText.setOnKeyListener(this)
    }

    override fun onOtpCompleted(otp: String?) {
        Toast.makeText(this, "OTP $otp", Toast.LENGTH_SHORT).show()
        makeRequest()
    }


    /**
     * Sets background of the view.
     * This method varies in implementation depending on Android SDK version.
     *
     * @param view       View to which set background
     * @param background Background to set to view
     */
    fun setViewBackground(view: View?, background: Drawable?) {
        if (view == null || background == null)
            return

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.background = background
        } else {
            view.setBackgroundDrawable(background)
        }
    }

    /**
     * Shows soft keyboard.
     *
     * @param editText EditText which has focus
     */
    fun showSoftKeyboard(editText: EditText?) {
        if (editText == null)
            return
        val imm = getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(editText, 0)
    }


//    inner class MainLayout(context: Context, attributeSet: AttributeSet?) : LinearLayout(context, attributeSet) {
//
//        init {
//            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//            inflater.inflate(R.layout.activity_sign_up_verification, this)
//        }
//
//        override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
//            val proposedHeight = View.MeasureSpec.getSize(heightMeasureSpec)
//            val actualHeight = height
//
//            Log.d("TAG", "proposed: $proposedHeight, actual: $actualHeight")
//
//            if (actualHeight >= proposedHeight) {
//                // Keyboard is shown
//                if (mPinHiddenEditText.length() == 0)
//                    setFocusedPinBackground(mPinFirstDigitEditText)
//                else
//                    setDefaultPinBackground(mPinFirstDigitEditText)
//            }
//
//            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
//        }
//    }
}
