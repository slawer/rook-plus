package com.rookery.rookplus.fragment

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.*
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.kittinunf.fuel.Fuel
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.miguelcatalan.materialsearchview.MaterialSearchView
import com.rookery.rookplus.R
import com.rookery.rookplus.adapter.*
import com.rookery.rookplus.model.*
import com.rookery.rookplus.util.APILinks
import dmax.dialog.SpotsDialog
import org.json.JSONArray
import org.json.JSONObject
import java.util.*


class HomeFragment : Fragment() , AppBarLayout.OnOffsetChangedListener {


    private lateinit var toolbar: Toolbar
    private lateinit var materialSearchView: MaterialSearchView
    private lateinit var collaspingToolbar: CollapsingToolbarLayout
    private lateinit var appBarLayout: AppBarLayout

    lateinit var discoverArrayList: ArrayList<DiscoverModel>
    lateinit var pollArrayList: ArrayList<PollModel>
    lateinit var eventArrayList: ArrayList<EventModel>
    lateinit var forYouArrayList: ArrayList<ForYouModel>
    lateinit var videoArrayList: ArrayList<VideoModel>

    lateinit var discoverAdapter: DiscoverAdapter
    lateinit var pollAdapter: PollAdapter
    lateinit var eventAdapter: EventAdapter
    lateinit var forYouAdapter: ForYouAdapter
    lateinit var videoAdapter: VideoAdapter

    lateinit var recyclerDiscover: RecyclerView
    lateinit var recyclerPolls: RecyclerView
    lateinit var recyclerEvent: RecyclerView
    lateinit var recyclerForYou: RecyclerView
    lateinit var recyclerVideo: RecyclerView

    private lateinit var prefs: SharedPreferences
    private lateinit var edit: SharedPreferences.Editor

    private lateinit var dialog: SpotsDialog

    var isShow: Boolean = false
    var scrollRange = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }


    @SuppressLint("WrongConstant")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_home, container, false)

        dialog = SpotsDialog.Builder().setContext(context).build() as SpotsDialog


        collaspingToolbar = rootView.findViewById(R.id.collaspingToolbar)
        toolbar = collaspingToolbar.findViewById(R.id.toolbar1)
        appBarLayout = rootView.findViewById(R.id.appBarLayout)

        recyclerDiscover = rootView.findViewById(R.id.recyclerDiscover)
        recyclerPolls = rootView.findViewById(R.id.recyclerPolls)
        recyclerEvent = rootView.findViewById(R.id.recyclerEvent)
        recyclerForYou = rootView.findViewById(R.id.recyclerForYou)
        recyclerVideo = rootView.findViewById(R.id.recyclerVideo)

        toolbar.visibility = View.INVISIBLE
//        toolbar.title = "Hello"
//        toolbar.setTitleTextColor(Color.parseColor("#F44336"))
        toolbar.setNavigationIcon(R.drawable.mini_rook)

        prefs = PreferenceManager.getDefaultSharedPreferences(context)
        edit = prefs.edit()

        //  (activity as AppCompatActivity).supportActionBar!!.title = "jhgtyd"
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
//        (activity as AppCompatActivity).supportActionBar!!.title = "Material"
//        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"))
//        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(true)
        //     (activity as AppCompatActivity).supportActionBar!!.title = "Title"
//        if (toolbar != null) {
//            (activity as AppCompatActivity).setSupportActionBar(toolbar)
//
//            val actionBar = (activity as AppCompatActivity).supportActionBar
//            if (actionBar != null) {
//                actionBar.setDisplayHomeAsUpEnabled(true)
//                actionBar.setDisplayShowTitleEnabled(true)
//                actionBar.setTitle("Transfer Funds Via Mobile Wallet")
//                actionBar.setDisplayUseLogoEnabled(false)
//                actionBar.setHomeButtonEnabled(true)
//            }
//        }

        discoverArrayList = ArrayList()
        pollArrayList = ArrayList()
        eventArrayList = ArrayList()
        forYouArrayList = ArrayList()
        videoArrayList = ArrayList()

        discoverAdapter = DiscoverAdapter(discoverArrayList, context)
        pollAdapter = PollAdapter(pollArrayList, context)
        eventAdapter = EventAdapter(eventArrayList, context)
        forYouAdapter = ForYouAdapter(forYouArrayList, context)
        videoAdapter = VideoAdapter(videoArrayList, context)

        recyclerDiscover.setHasFixedSize(true)
        recyclerPolls.setHasFixedSize(true)
        recyclerEvent.setHasFixedSize(true)
        recyclerForYou.setHasFixedSize(true)
        recyclerVideo.setHasFixedSize(true)

        val llm = LinearLayoutManager(context)
        val llm1 = LinearLayoutManager(context)
        val llm2 = LinearLayoutManager(context)
        val llm3 = LinearLayoutManager(context)
        val llm4 = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.HORIZONTAL
        llm1.orientation = LinearLayoutManager.HORIZONTAL
        llm2.orientation = LinearLayoutManager.VERTICAL
        llm3.orientation = LinearLayoutManager.VERTICAL
        llm4.orientation = LinearLayoutManager.VERTICAL
        recyclerDiscover.layoutManager = llm
        recyclerPolls.layoutManager = llm1
        recyclerEvent.layoutManager = llm2
        recyclerForYou.layoutManager = llm3
        recyclerVideo.layoutManager = llm4
        //recyclerAssignedReq.itemAnimator = SlideInUpAnimator(OvershootInterpolator(1f))
        //recyclerAssignedReq.itemAnimator = DefaultItemAnimator()

        recyclerDiscover.adapter = discoverAdapter
        recyclerPolls.adapter = pollAdapter
        recyclerEvent.adapter = eventAdapter
        recyclerForYou.adapter = forYouAdapter
        recyclerVideo.adapter = videoAdapter



        appBarLayout.addOnOffsetChangedListener(this)

        materialSearchView = rootView.findViewById(
            R.id.searchView
        )


        getDiscoverList()
        getPollList()
        getEventList()
        getForYouList()
        getVideoList()

        return rootView
    }

    fun getDiscoverList() {
        dialog.setMessage("Loading...")
        dialog.show()
        val timeout = 5000 // 5000 milliseconds = 5 seconds.
        val timeoutRead = 60000 // 60000 milliseconds = 1 minute.


        Fuel.get(APILinks.DISCOVER_LIST_URL).header("x-ROOK-token" to prefs.getString("user_jwt","")).timeout(timeout).timeoutRead(timeoutRead).responseString { request, response, result ->
            //            println(request)
//            println(response)
            val (data, error) = result
            println(request.httpString())
            if (error == null) {
                println(data.toString())
                //do something when success
                val jO = JSONObject(data.toString())

                val jsonInner = JSONObject(jO.getString("result"))
                val discoverObj = JSONArray(jsonInner.getString("discoveries"))

                discoverArrayList.clear()

                var discoverModel: DiscoverModel
                for (i in 0 until discoverObj.length()) {
                    val rnd = Random()
                    val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

                    val escalationObject = discoverObj.getJSONObject(i)
                    val id = escalationObject.getString("id")
                    val image_url = escalationObject.getString("image_url")
                    val type = escalationObject.getString("type")
                    val target_id = escalationObject.getString("target_id")


                    // val strBool = status.toString()
                    //  Log.e("strBool", strBool)


                    discoverModel = DiscoverModel(id, image_url, type, target_id)


                    discoverArrayList.add(discoverModel)

                }
                dialog.dismiss()
                discoverAdapter.notifyDataSetChanged()
            } else {
                //error handling
            }


        }
    }

    fun getPollList() {
        dialog.setMessage("Loading...")
        dialog.show()
        val timeout = 5000 // 5000 milliseconds = 5 seconds.
        val timeoutRead = 60000 // 60000 milliseconds = 1 minute.


        Fuel.get(APILinks.POLL_LIST_URL).header("x-ROOK-token" to prefs.getString("user_jwt","")).timeout(timeout).timeoutRead(timeoutRead).responseString { request, response, result ->
            //            println(request)
//            println(response)
            val (data, error) = result
            println(request.httpString())
            if (error == null) {
                println(data.toString())
                //do something when success
                val jO = JSONObject(data.toString())

                val jsonInner = JSONObject(jO.getString("result"))
                val pollObj = JSONArray(jsonInner.getString("polls"))

                pollArrayList.clear()

                var pollModel: PollModel
                for (i in 0 until pollObj.length()) {
                    val rnd = Random()
                    val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

                    val resultObject = pollObj.getJSONObject(i)
                    val id = resultObject.getString("id")
                    val options = resultObject.getString("options")
                    val timepost = resultObject.getString("timepost")
                    val cname = resultObject.getString("cname")
                    val logo = resultObject.getString("logo")
                    val title = resultObject.getString("title")
                    val result = resultObject.getString("result")


                    // val strBool = status.toString()
                    //  Log.e("strBool", strBool)


                    pollModel = PollModel(id, title,options, timepost, cname,logo, result)


                    pollArrayList.add(pollModel)

                }
                dialog.dismiss()
                pollAdapter.notifyDataSetChanged()
            } else {
                //error handling
            }


        }
    }

    fun getEventList() {
        dialog.setMessage("Loading...")
        dialog.show()
        val timeout = 5000 // 5000 milliseconds = 5 seconds.
        val timeoutRead = 60000 // 60000 milliseconds = 1 minute.


        Fuel.get(APILinks.EVENT_LIST_URL).header("x-ROOK-token" to prefs.getString("user_jwt","")).timeout(timeout).timeoutRead(timeoutRead).responseString { request, response, result ->
            //            println(request)
//            println(response)
            val (data, error) = result
            println(request.httpString())
            if (error == null) {
                println(data.toString())
                //do something when success
                val jO = JSONObject(data.toString())

                val jsonInner = JSONObject(jO.getString("result"))
                val eventObj = JSONArray(jsonInner.getString("events"))

                eventArrayList.clear()

                var eventModel: EventModel
                for (i in 0 until eventObj.length()) {
                    val rnd = Random()
                    val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

                    val resultObject = eventObj.getJSONObject(i)
                    val id = resultObject.getString("id")
                    val image = resultObject.getString("image")
                    val price = resultObject.getString("price")
                    val title = resultObject.getString("title")
                    val timepost = resultObject.getString("timepost")
                    val location = resultObject.getString("location")
                    val event_date = resultObject.getString("event_date")
                    val details = resultObject.getString("details")
                    val cname = resultObject.getString("cname")
                    val logo = resultObject.getString("logo")
                    val category = resultObject.getString("category")


                    // val strBool = status.toString()
                    //  Log.e("strBool", strBool)


                    eventModel = EventModel(id,image, price, title, timepost, location, event_date, details, cname, logo, category)


                    eventArrayList.add(eventModel)

                }
                dialog.dismiss()
                eventAdapter.notifyDataSetChanged()
            } else {
                //error handling
            }


        }
    }

    fun getForYouList() {
        dialog.setMessage("Loading...")
        dialog.show()
        val timeout = 5000 // 5000 milliseconds = 5 seconds.
        val timeoutRead = 60000 // 60000 milliseconds = 1 minute.


        Fuel.get(APILinks.ARTICLE_LIST_URL).header("x-ROOK-token" to prefs.getString("user_jwt","")).timeout(timeout).timeoutRead(timeoutRead).responseString { request, response, result ->
            //            println(request)
//            println(response)
            val (data, error) = result
            println(request.httpString())
            if (error == null) {
                println(data.toString())
                //do something when success
                val jO = JSONObject(data.toString())

                val jsonInner = JSONObject(jO.getString("result"))
                val articleObj = JSONArray(jsonInner.getString("articles"))

                forYouArrayList.clear()

                var forYouModel: ForYouModel
                for (i in 0 until articleObj.length()) {
                    val rnd = Random()
                    val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

                    val resultObject = articleObj.getJSONObject(i)
                    val id = resultObject.getString("id")
                    val views = resultObject.getString("views")
                    val link = resultObject.getString("link")
                    val title = resultObject.getString("title")
                    val timepost = resultObject.getString("timepost")
                    val cname = resultObject.getString("cname")
                    val logo = resultObject.getString("logo")
                    val category = resultObject.getString("category")


                    // val strBool = status.toString()
                    //  Log.e("strBool", strBool)


                    forYouModel = ForYouModel(id,views, link, title, timepost, cname, logo, category)


                    forYouArrayList.add(forYouModel)

                }
                dialog.dismiss()
                forYouAdapter.notifyDataSetChanged()
            } else {
                //error handling
            }


        }
    }

    fun getVideoList() {
        dialog.setMessage("Loading...")
        dialog.show()
        val timeout = 5000 // 5000 milliseconds = 5 seconds.
        val timeoutRead = 60000 // 60000 milliseconds = 1 minute.


        Fuel.get(APILinks.VIDEO_LIST_URL).header("x-ROOK-token" to prefs.getString("user_jwt","")).timeout(timeout).timeoutRead(timeoutRead).responseString { request, response, result ->
            //            println(request)
//            println(response)
            val (data, error) = result
            println(request.httpString())
            if (error == null) {
                println(data.toString())
                //do something when success
                val jO = JSONObject(data.toString())

                val jsonInner = JSONObject(jO.getString("result"))
                val articleObj = JSONArray(jsonInner.getString("videos"))

                videoArrayList.clear()

                var videoModel: VideoModel
                for (i in 0 until articleObj.length()) {
                    val rnd = Random()
                    val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

                    val resultObject = articleObj.getJSONObject(i)
                    val id = resultObject.getString("id")
                    val views = resultObject.getString("views")
                    val link = resultObject.getString("link")
                    val title = resultObject.getString("title")
                    val timepost = resultObject.getString("timepost")
                    val cname = resultObject.getString("cname")
                    val logo = resultObject.getString("logo")
                    val category = resultObject.getString("category")


                    // val strBool = status.toString()
                    //  Log.e("strBool", strBool)


                    videoModel = VideoModel(id,views, link, title, timepost, cname, logo, category)


                    videoArrayList.add(videoModel)

                }
                dialog.dismiss()
                videoAdapter.notifyDataSetChanged()
            } else {
                //error handling
            }


        }
    }

    override fun onOffsetChanged(p0: AppBarLayout?, verticalOffset: Int) {


        if (scrollRange == -1) {
            scrollRange = appBarLayout.totalScrollRange
        }
        if (scrollRange + verticalOffset == 0) {
            collaspingToolbar.title = "Home"
            isShow = true
            toolbar.visibility = View.VISIBLE

            setHasOptionsMenu(true)
        } else if (isShow) {
            collaspingToolbar.title = ""
            isShow = false
            setHasOptionsMenu(false)
            toolbar.visibility = View.GONE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {

        super.onCreateOptionsMenu(menu, inflater)



        inflater!!.inflate(R.menu.menu_company, menu)
        val menuItem: MenuItem = menu!!.findItem(R.id.searchMenu)

        val searchView = menuItem.actionView as SearchView
        searchView.imeOptions = EditorInfo.IME_ACTION_DONE
        // searchView.setBackgroundDrawable(resources.getDrawable(R.drawable.round_edittext_sec))
        // searchView.setBackgroundColor(Color.parseColor("#ffffff"))
        //materialSearchView.setMenuItem(menuItem)
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            HomeFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}

