package com.rookery.rookplus.fragment

import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.*
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.kittinunf.fuel.Fuel
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.miguelcatalan.materialsearchview.MaterialSearchView
import com.rookery.rookplus.R
import com.rookery.rookplus.adapter.InternshipAdapter
import com.rookery.rookplus.adapter.RecommendedIntershipAdapter
import com.rookery.rookplus.model.InternshipModel
import com.rookery.rookplus.model.RecInternshipModel
import com.rookery.rookplus.util.APILinks
import dmax.dialog.SpotsDialog
import org.json.JSONArray
import org.json.JSONObject
import java.util.*


class InternHomeFragment : Fragment(), AppBarLayout.OnOffsetChangedListener {

    private lateinit var toolbar: Toolbar
    private lateinit var materialSearchView: MaterialSearchView
    private lateinit var collaspingToolbar: CollapsingToolbarLayout
    private lateinit var appBarLayout: AppBarLayout

    private lateinit var prefs: SharedPreferences
    private lateinit var edit: SharedPreferences.Editor

    private lateinit var dialog: SpotsDialog

    lateinit var recInternshipArrayList: ArrayList<RecInternshipModel>
    lateinit var internshipArrayList: ArrayList<InternshipModel>

    lateinit var recommendedIntershipAdapter: RecommendedIntershipAdapter
    lateinit var internshipAdapter: InternshipAdapter

    lateinit var recyclerRecommendedInternships: RecyclerView
    lateinit var recyclerAllInternships: RecyclerView

    var isShow: Boolean = false
    var scrollRange = -1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_intern_home, container, false)

        dialog = SpotsDialog.Builder().setContext(context).build() as SpotsDialog

        prefs = PreferenceManager.getDefaultSharedPreferences(context)
        edit = prefs.edit()

        collaspingToolbar = rootView.findViewById(R.id.collaspingToolbar)
        toolbar = collaspingToolbar.findViewById(R.id.toolbar1)
        appBarLayout = rootView.findViewById(R.id.appBarLayout)

        recInternshipArrayList = java.util.ArrayList()
        internshipArrayList = java.util.ArrayList()

        recommendedIntershipAdapter = RecommendedIntershipAdapter(recInternshipArrayList, context)
        internshipAdapter = InternshipAdapter(internshipArrayList, context)

        recyclerRecommendedInternships = rootView.findViewById(R.id.recyclerRecommendedInternships)
        recyclerAllInternships = rootView.findViewById(R.id.recyclerAllInternships)

        toolbar.visibility = View.INVISIBLE
        toolbar.setNavigationIcon(R.drawable.mini_rook)


        (activity as AppCompatActivity).setSupportActionBar(toolbar)


        appBarLayout.addOnOffsetChangedListener(this)

        materialSearchView = rootView.findViewById(
            R.id.searchView
        )


        recyclerRecommendedInternships.setHasFixedSize(true)
        recyclerAllInternships.setHasFixedSize(true)

        val llm = LinearLayoutManager(context)
        val llm2 = LinearLayoutManager(context)

        llm.orientation = LinearLayoutManager.HORIZONTAL
        llm2.orientation = LinearLayoutManager.VERTICAL

        recyclerRecommendedInternships.layoutManager = llm
        recyclerAllInternships.layoutManager = llm2
        //recyclerAssignedReq.itemAnimator = SlideInUpAnimator(OvershootInterpolator(1f))
        //recyclerAssignedReq.itemAnimator = DefaultItemAnimator()

        recyclerRecommendedInternships.adapter = recommendedIntershipAdapter
        recyclerAllInternships.adapter = internshipAdapter

        getRecInternships()
        getInternships()

        return rootView
    }

    fun getRecInternships() {
//        dialog.setMessage("Loading...")
//        dialog.show()
        val timeout = 5000 // 5000 milliseconds = 5 seconds.
        val timeoutRead = 60000 // 60000 milliseconds = 1 minute.


        Fuel.get(APILinks.REC_INTERNSHIP_LIST_URL).header("x-ROOK-token" to prefs.getString("user_jwt", ""))
            .timeout(timeout)
            .timeoutRead(timeoutRead).responseString { request, response, result ->
                //            println(request)
//            println(response)
                val (data, error) = result
                println("Request Made " + request.httpString())
                if (error == null) {
                    println("Recommended Internship Results" + data.toString())
                    //do something when success
                    val jO = JSONObject(data.toString())

                    val jsonInner = JSONObject(jO.getString("result"))
                    val internshipArray = JSONArray(jsonInner.getString("internships"))


                    recInternshipArrayList.clear()

                    var recInternshipModel: RecInternshipModel

                    for (i in 0 until internshipArray.length()) {
                        val rnd = Random()
                        val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

                            val internshipObj = internshipArray.getJSONObject(i)
                            //user info
                            val id = internshipObj.getString("id")
                            val type = internshipObj.getString("type")
                            val location = internshipObj.getString("location")
                            val title = internshipObj.getString("title")
                            val deadline = internshipObj.getString("deadline")
                            val cname = internshipObj.getString("cname")
                            val logo = internshipObj.getString("logo")
                            val category = internshipObj.getString("category")
                            val is_applied = internshipObj.getString("is_applied")
                            val priority = internshipObj.getString("priority")

//                        recInternshipModel = RecInternshipModel(
//                            "East Legon",
//                            "",
//                            "East Legon",
//                            "free",
//                            "",
//                            "4 Byte Gh",
//                            "https://myrookery.com/img/avatar/rookCompa012dcd9e001b53.jpg",
//                            "Design",
//                            "0",
//                            ""
//                        )

                            recInternshipModel = RecInternshipModel(
                                title,
                                id,
                                location,
                                type,
                                deadline,
                                cname,
                                logo,
                                category,
                                is_applied,
                                priority.toString()
                            )


                        recInternshipArrayList.add(recInternshipModel)
                    }


                    dialog.dismiss()
                    recommendedIntershipAdapter.notifyDataSetChanged()
                } else {
                    //error handling
                }


            }
    }

    fun getInternships() {
//        dialog.setMessage("Loading...")
//        dialog.show()
        val timeout = 5000 // 5000 milliseconds = 5 seconds.
        val timeoutRead = 60000 // 60000 milliseconds = 1 minute.


        Fuel.get(APILinks.INTERNSHIP_LIST_URL).header("x-ROOK-token" to prefs.getString("user_jwt", ""))
            .timeout(timeout)
            .timeoutRead(timeoutRead).responseString { request, response, result ->
                //            println(request)
//            println(response)
                val (data, error) = result
                println("Request Made " + request.httpString())
                if (error == null) {
                    println("Internship Results" + data.toString())
                    //do something when success
                    val jO = JSONObject(data.toString())

                    val jsonInner = JSONObject(jO.getString("result"))
                    val internshipArray = JSONArray(jsonInner.getString("internships"))


                    internshipArrayList.clear()

                    var internshipModel: InternshipModel

                    for (i in 0 until internshipArray.length()) {
                        val rnd = Random()
                        val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

                            val internshipObj = internshipArray.getJSONObject(i)
                            //user info
                            val id = internshipObj.getString("id")
                            val type = internshipObj.getString("type")
                            val location = internshipObj.getString("location")
                            val title = internshipObj.getString("title")
                            val deadline = internshipObj.getString("deadline")
                            val cname = internshipObj.getString("cname")
                            val logo = internshipObj.getString("logo")
                            val category = internshipObj.getString("category")
                            val is_applied = internshipObj.getString("is_applied")


                            internshipModel = InternshipModel(
                                title,
                                id,
                                location,
                                type,
                                deadline,
                                cname,
                                logo,
                                category,
                                is_applied
                            )


                        internshipArrayList.add(internshipModel)
                    }


                    dialog.dismiss()
                    internshipAdapter.notifyDataSetChanged()
                } else {
                    //error handling
                }


            }
    }

        override fun onOffsetChanged(p0: AppBarLayout?, verticalOffset: Int) {


            if (scrollRange == -1) {
                scrollRange = appBarLayout.totalScrollRange
            }
            if (scrollRange + verticalOffset == 0) {
              //  collaspingToolbar.title = "Companies"
                isShow = true
                toolbar.visibility = View.VISIBLE

                setHasOptionsMenu(true)
            } else if (isShow) {
               // collaspingToolbar.title = ""
                isShow = false
                setHasOptionsMenu(false)
                toolbar.visibility = View.GONE
            }
        }

        override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {

            super.onCreateOptionsMenu(menu, inflater)



            inflater!!.inflate(R.menu.menu_company, menu)
            val menuItem: MenuItem = menu!!.findItem(R.id.searchMenu)

            val searchView = menuItem.actionView as SearchView
            searchView.imeOptions = EditorInfo.IME_ACTION_DONE
            // searchView.setBackgroundDrawable(resources.getDrawable(R.drawable.round_edittext_sec))
            // searchView.setBackgroundColor(Color.parseColor("#ffffff"))
            //materialSearchView.setMenuItem(menuItem)
        }

        companion object {

            @JvmStatic
            fun newInstance() =
                InternHomeFragment().apply {
                    arguments = Bundle().apply {

                    }
                }
        }
    }
