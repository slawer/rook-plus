package com.rookery.rookplus.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.github.kittinunf.fuel.Fuel
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.miguelcatalan.materialsearchview.MaterialSearchView
import com.rookery.rookplus.R
import com.rookery.rookplus.activity.UserInterestsUpdateActivity
import com.rookery.rookplus.activity.UserSkillsUpdateActivity
import com.rookery.rookplus.adapter.ProEducationAdapter
import com.rookery.rookplus.adapter.ProPortfolioAdapter
import com.rookery.rookplus.adapter.ProWorkExperienceAdapter
import com.rookery.rookplus.model.*
import com.rookery.rookplus.util.APILinks
import com.sasank.roundedhorizontalprogress.RoundedHorizontalProgressBar
import de.hdodenhof.circleimageview.CircleImageView
import dmax.dialog.SpotsDialog
import org.json.JSONArray
import org.json.JSONObject
import java.util.*


class ProfileFragment : Fragment(), View.OnClickListener, AppBarLayout.OnOffsetChangedListener {

    private lateinit var toolbar: Toolbar
    private lateinit var materialSearchView: MaterialSearchView
    private lateinit var collaspingToolbar: CollapsingToolbarLayout
    private lateinit var appBarLayout: AppBarLayout


    lateinit var demographicsArrayList: ArrayList<DemographicsModel>
    lateinit var educationArrayList: ArrayList<EducationModel>
    lateinit var portfolioArrayList: ArrayList<PortfolioModel>
    lateinit var aptitudeArrayList: ArrayList<AptitudeModel>
    lateinit var interestArrayList: ArrayList<InterestsModel>
    lateinit var statsArrayList: ArrayList<StatsModel>
    lateinit var workArrayList: ArrayList<WorkExpModel>
    lateinit var skillsArrayList: ArrayList<SkillsModel>

    private lateinit var prefs: SharedPreferences
    private lateinit var edit: SharedPreferences.Editor

    private lateinit var dialog: SpotsDialog

    private lateinit var txtNationality: TextView
    private lateinit var txtTasks: TextView
    private lateinit var txtCompleted: TextView
    private lateinit var txtBadges: TextView
    private lateinit var txtGender: TextView
    private lateinit var txtAge: TextView
    private lateinit var txtEmploymentStatus: TextView
    private lateinit var txtCity: TextView
    private lateinit var txtMaritalStatus: TextView
    private lateinit var txtTestTaken: TextView
    private lateinit var txtHighestScore: TextView
    private lateinit var txtAverageScore: TextView
    private lateinit var txtPercentile: TextView
    private lateinit var txtPersonName: TextView

    private lateinit var imgUserAvatar: CircleImageView

    private lateinit var updateDemographicsBtn: LinearLayout
    private lateinit var updateWorkExperienceBtn: LinearLayout
    private lateinit var updateEducationBtn: LinearLayout
    private lateinit var updateSkillsBtn: LinearLayout
    private lateinit var updateInterestBtn: LinearLayout
    private lateinit var updatePortfolioBtn: LinearLayout
    private lateinit var layoutBadges: LinearLayout

    var isShow: Boolean = false
    var scrollRange = -1

    lateinit var demographicsModel: DemographicsModel
    lateinit var educationModel: EducationModel
    lateinit var portfolioModel: PortfolioModel
    lateinit var workExpModel: WorkExpModel
    lateinit var statsModel: StatsModel
    lateinit var skillsModel: SkillsModel
    lateinit var aptitudeModel: AptitudeModel
    lateinit var interestsModel: InterestsModel

    lateinit var spSuccess: RoundedHorizontalProgressBar
    lateinit var spSpeed: RoundedHorizontalProgressBar

    lateinit var recyclerEducation: RecyclerView
    lateinit var recyclerWorkExperience: RecyclerView
    lateinit var recyclerPortfolio: RecyclerView

    lateinit var proEducationAdapter: ProEducationAdapter
    lateinit var proWorkExpAdapter: ProWorkExperienceAdapter
    lateinit var proPortfolioAdapter: ProPortfolioAdapter

    lateinit var skill_chip_group: ChipGroup
    lateinit var interest_chip_group: ChipGroup


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    @SuppressLint("WrongConstant")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_profile, container, false)

        dialog = SpotsDialog.Builder().setContext(context).build() as SpotsDialog

        prefs = PreferenceManager.getDefaultSharedPreferences(context)
        edit = prefs.edit()

        collaspingToolbar = rootView.findViewById(R.id.collaspingToolbar)
        toolbar = collaspingToolbar.findViewById(R.id.toolbar1)
        appBarLayout = rootView.findViewById(R.id.appBarLayout)

        toolbar.visibility = View.INVISIBLE
        toolbar.setNavigationIcon(R.drawable.mini_rook)

        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        demographicsArrayList = java.util.ArrayList()
        educationArrayList = java.util.ArrayList()
        portfolioArrayList = java.util.ArrayList()
        workArrayList = java.util.ArrayList()
        statsArrayList = java.util.ArrayList()
        interestArrayList = java.util.ArrayList()
        aptitudeArrayList = java.util.ArrayList()
        skillsArrayList = java.util.ArrayList()


        recyclerEducation = rootView.findViewById(R.id.recyclerEducation)
        recyclerWorkExperience = rootView.findViewById(R.id.recyclerWorkExperience)
        recyclerPortfolio = rootView.findViewById(R.id.recyclerPortfolio)


        spSpeed = rootView.findViewById(R.id.pbSpeed)
        spSuccess = rootView.findViewById(R.id.pbSuccessRate)

        skill_chip_group = rootView.findViewById(R.id.skill_chip_group)
        interest_chip_group = rootView.findViewById(R.id.interest_chip_group)

        updateDemographicsBtn = rootView.findViewById(R.id.updateDemographicsBtn)
        updateEducationBtn = rootView.findViewById(R.id.updateEducationBtn)
        updateWorkExperienceBtn = rootView.findViewById(R.id.updateWorkExperienceBtn)
        updateInterestBtn = rootView.findViewById(R.id.updateInterestBtn)
        updateSkillsBtn = rootView.findViewById(R.id.updateSkillsBtn)
        updatePortfolioBtn = rootView.findViewById(R.id.updatePortfolioBtn)
        layoutBadges = rootView.findViewById(R.id.layoutBadges)


        txtNationality = rootView.findViewById(R.id.txtNationality)
        txtTasks = rootView.findViewById(R.id.txtTasks)
        txtCompleted = rootView.findViewById(R.id.txtCompleted)
        txtBadges = rootView.findViewById(R.id.txtBadges)
        txtGender = rootView.findViewById(R.id.txtGender)
        txtAge = rootView.findViewById(R.id.txtAge)
        txtEmploymentStatus = rootView.findViewById(R.id.txtEmploymentStatus)
        txtCity = rootView.findViewById(R.id.txtCity)
        txtMaritalStatus = rootView.findViewById(R.id.txtMaritalStatus)
        txtTestTaken = rootView.findViewById(R.id.txtTestTaken)
        txtHighestScore = rootView.findViewById(R.id.txtHighestScore)
        txtAverageScore = rootView.findViewById(R.id.txtAverageScore)
        txtPercentile = rootView.findViewById(R.id.txtPercentile)
        txtPersonName = rootView.findViewById(R.id.txtPersonName)

        imgUserAvatar = rootView.findViewById(R.id.imgUserAvatar)

        proEducationAdapter = ProEducationAdapter(educationArrayList, context)
        proWorkExpAdapter = ProWorkExperienceAdapter(workArrayList, context)
        proPortfolioAdapter = ProPortfolioAdapter(portfolioArrayList, context)

        appBarLayout.addOnOffsetChangedListener(this)

        materialSearchView = rootView.findViewById(
            R.id.searchView
        )

        updateDemographicsBtn.setOnClickListener(this)
        updateWorkExperienceBtn.setOnClickListener(this)
        updateEducationBtn.setOnClickListener(this)
        updateInterestBtn.setOnClickListener(this)
        updateSkillsBtn.setOnClickListener(this)
        updatePortfolioBtn.setOnClickListener(this)

        recyclerEducation.setHasFixedSize(true)
        recyclerWorkExperience.setHasFixedSize(true)
        recyclerPortfolio.setHasFixedSize(true)

        val llm = LinearLayoutManager(context)
        val llm2 = LinearLayoutManager(context)
        val llm3 = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        llm2.orientation = LinearLayoutManager.VERTICAL
        llm3.orientation = LinearLayoutManager.VERTICAL

        recyclerEducation.layoutManager = llm
        recyclerWorkExperience.layoutManager = llm2
        recyclerPortfolio.layoutManager = llm3

        recyclerEducation.adapter = proEducationAdapter
        recyclerWorkExperience.adapter = proWorkExpAdapter
        recyclerPortfolio.adapter = proPortfolioAdapter

        getUserProfile()



        return rootView
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.updateDemographicsBtn -> {
                    Toast.makeText(context, "Demographics", Toast.LENGTH_LONG).show()
                }

                R.id.updateWorkExperienceBtn -> {
                    Toast.makeText(context, "Work Experience", Toast.LENGTH_LONG).show()
                }

                R.id.updateEducationBtn -> {
                    Toast.makeText(context, "Education", Toast.LENGTH_LONG).show()
                }

                R.id.updateInterestBtn -> {
                    Toast.makeText(context, "Interest", Toast.LENGTH_LONG).show()
                    getInterestChip()
                    val intent = Intent(context,UserInterestsUpdateActivity::class.java)
                    startActivity(intent)
                }

                R.id.updateSkillsBtn -> {
                    Toast.makeText(context, "Skills", Toast.LENGTH_LONG).show()
                    getSkillChip()
                    val intent = Intent(context,UserSkillsUpdateActivity::class.java)
                    startActivity(intent)
                }

                R.id.updatePortfolioBtn -> {
                    Toast.makeText(context, "Portfolio", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    @SuppressLint("ResourceType")
    fun getUserProfile() {
        dialog.setMessage("Loading...")
        dialog.show()
        val timeout = 5000 // 5000 milliseconds = 5 seconds.
        val timeoutRead = 60000 // 60000 milliseconds = 1 minute.


        Fuel.get(APILinks.GET_USER_PRO_URL).header("x-ROOK-token" to prefs.getString("user_jwt", "")).timeout(timeout)
            .timeoutRead(timeoutRead).responseString { request, response, result ->
                //            println(request)
//            println(response)
                val (data, error) = result
                println("Request Made " + request.httpString())
                if (error == null) {
                    println("Profile Results" + data.toString())
                    //do something when success
                    val jO = JSONObject(data.toString())

                    val jsonInner = JSONObject(jO.getString("result"))
                    val userObj = JSONObject(jsonInner.getString("user"))
                    val interestsArray = JSONArray(userObj.getString("interests"))
                    val educationArray = JSONArray(userObj.getString("education"))
                    val experienceArray = JSONArray(userObj.getString("experience"))
                    val statsObj = JSONObject(userObj.getString("stats"))
                    val skillsArray = JSONArray(userObj.getString("skills"))
                    val portfolioArray = JSONArray(userObj.getString("portfolio"))
                    val aptitudeObj = JSONObject(userObj.getString("aptitude"))

                    demographicsArrayList.clear()
                    educationArrayList.clear()
                    portfolioArrayList.clear()
                    workArrayList.clear()
                    interestArrayList.clear()
                    aptitudeArrayList.clear()
                    statsArrayList.clear()
                    skillsArrayList.clear()


                    //user info
                    val fname = userObj.getString("fname")
                    val lname = userObj.getString("lname")
                    val gender = userObj.getString("gender")
                    val dob = userObj.getString("dob")
                    val city = userObj.getString("city")
                    val nationality = userObj.getString("nationality")
                    val employment_status = userObj.getString("employment_status")
                    val marital_status = userObj.getString("marital_status")
                    val phone = userObj.getString("phone")
                    val avatar = userObj.getString("avatar")
                    val email = userObj.getString("email")

                    //stats
                    val total_tasks = statsObj.getInt("total_tasks")
                    val completed = statsObj.getInt("completed")
                    val points = statsObj.getInt("points")
                    val success_rate = statsObj.getInt("success_rate")
                    val speed = statsObj.getInt("speed")
                    val badges = statsObj.getJSONArray("badges")

                    //aptitude
                    val tests_taken = aptitudeObj.getInt("tests_taken")
                    val highest_score = aptitudeObj.getInt("highest_score")
                    val average_score = aptitudeObj.getInt("average_score")
                    val percentile = aptitudeObj.getInt("percentile")

                    //adding data to models
                    aptitudeModel = AptitudeModel(
                        tests_taken.toString(),
                        highest_score.toString(), percentile.toString(), average_score.toString()
                    )
                    statsModel = StatsModel(
                        total_tasks.toString(),
                        completed.toString(),
                        points.toString(),
                        success_rate.toString(),
                        speed.toString(),
                        badges.toString()
                    )
                    Log.d("user_badges", badges.toString())
                    Log.d("user_badges", badges.toString())
                    Log.d("user_badges", badges.toString())
//                    demographicsModel = DemographicsModel(
//                        "",
//                        "",
//                        "",
//                        "",
//                        "",
//                        "",
//                        "",
//                        "",
//                        "",
//                        "",
//                        email
//                    )

                    demographicsModel = DemographicsModel(
                        gender,
                        nationality,
                        city,
                        employment_status,
                        marital_status,
                        dob,
                        fname,
                        lname,
                        phone,
                        avatar,
                        email
                    )

                    for (i in 0 until interestsArray.length()) {
                        val rnd = Random()
                        val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

                        val escalationObject = interestsArray.getJSONObject(i)
                        val title = escalationObject.getString("title")
                        val id = escalationObject.getString("id")

                        interestsModel = InterestsModel(title, id)


                        interestArrayList.add(interestsModel)
                    }

                    for (i in 0 until educationArray.length()) {
                        val rnd = Random()
                        val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

                        val escalationObject = educationArray.getJSONObject(i)
                        val name = escalationObject.getString("name")
                        val id = escalationObject.getString("id")
                        val completion = escalationObject.getString("completion")
                        val course = escalationObject.getString("course")
                        val level = escalationObject.getString("level")

                        educationModel = EducationModel(name, course, completion, level, id)


                        educationArrayList.add(educationModel)
                    }

                    for (i in 0 until experienceArray.length()) {
                        val rnd = Random()
                        val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

                        val escalationObject = experienceArray.getJSONObject(i)

                        val title = escalationObject.getString("title")
                        val id = escalationObject.getString("id")
                        val name = escalationObject.getString("name")
                        val location = escalationObject.getString("location")
                        val is_current = escalationObject.getString("is_current")
                        val start_date = escalationObject.getString("start_date")
                        val end_date = escalationObject.getString("end_date")

                        workExpModel = WorkExpModel(name, id, location, start_date, end_date, title, is_current)


                        workArrayList.add(workExpModel)
                    }

                    for (i in 0 until skillsArray.length()) {
                        val rnd = Random()
                        val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

                        val escalationObject = skillsArray.getJSONObject(i)

                        val title = escalationObject.getString("title")
                        val id = escalationObject.getString("id")

                        skillsModel = SkillsModel(title, id)


                        skillsArrayList.add(skillsModel)
                    }

                    for (i in 0 until portfolioArray.length()) {
                        val rnd = Random()
                        val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

                        val escalationObject = portfolioArray.getJSONObject(i)

                        val title = escalationObject.getString("title")
                        val id = escalationObject.getString("id")
                        val description = escalationObject.getString("description")
                        val start_date = escalationObject.getString("start_date")
                        val end_date = escalationObject.getString("end_date")
                        val items = escalationObject.getJSONArray("items")

                        portfolioModel = PortfolioModel(title, id, description, start_date, end_date, items.toString())

                        Log.d("user_port_items", items.toString())
                        portfolioArrayList.add(portfolioModel)
                    }

                    aptitudeArrayList.add(aptitudeModel)
                    statsArrayList.add(statsModel)
                    demographicsArrayList.add(demographicsModel)




                    txtNationality.text = demographicsModel.nationality
                    txtTasks.text = statsModel.total_tasks
                    txtCompleted.text = statsModel.completed

                    if (demographicsModel.gender == "m")
                        txtGender.text = "Male"
                    else {
                        txtGender.text = "Female"
                    }
                    txtPersonName.text = demographicsModel.fname +" "+ demographicsModel.lname

                    //txtAge.text = demographicsModel.age
                    txtEmploymentStatus.text = demographicsModel.emp_status
                    txtCity.text = demographicsModel.city
                    txtMaritalStatus.text = demographicsModel.martital_status
                    spSpeed.progress = statsModel.speed.toInt()
                    spSuccess.progress = statsModel.success.toInt()

                    txtTestTaken.text = aptitudeModel.tests_taken
                    txtHighestScore.text = aptitudeModel.highest_score
                    txtAverageScore.text = aptitudeModel.average_score
                    txtPercentile.text = aptitudeModel.percentile

                    Glide
                        .with(this@ProfileFragment)
                        .load(demographicsModel.avatar)
                        .centerCrop()
                        .placeholder(R.drawable.image_loading)
                        .into(imgUserAvatar)

                    val badge_length = JSONArray(statsModel.badges)

                    txtBadges.text = badge_length.length().toString()
                    Log.d("badge count", badge_length.length().toString())

                    for (i in 0 until badge_length.length()) // where x is the size of the list containing your alphabet.
                    {
                        val lp =  LinearLayout.LayoutParams(
                            100,
                            100
                        )
                        lp.setMargins(20, 10, 20, 10);
                        val button = ImageView(context)
                        button.layoutParams = lp
                        button.setImageDrawable(ContextCompat.getDrawable(this.context!!, R.drawable.build_profile))
                        button.maxHeight = 100
                        button.maxWidth = 100
                        button.scaleType = ImageView.ScaleType.FIT_CENTER




                        layoutBadges.addView(button)
                    }

                    println("Skill Array List $skillsArrayList")
                    postDefaultInterest()
                    postDefaultSkills()

                    dialog.dismiss()
                    //companyAdapter.notifyDataSetChanged()
                } else {
                    //error handling
                }


            }
    }


    override fun onResume() {
        super.onResume()
   //     getUserProfile()
    }

    fun postDefaultSkills() {
        val default_skills =
            "php music dance singing coding running jogging".split(" ".toRegex()).dropLastWhile { it.isEmpty() }
                .toTypedArray()

        val inflater = LayoutInflater.from(context)
        for (text in default_skills) {
            val chip_item = inflater.inflate(R.layout.skills_chip_item, null, false) as Chip
            chip_item.text = text
//                chip_item.setOnClickListener {view->
//                    chip_group.removeView(view)
//                }
            skill_chip_group.addView(chip_item)

        }
    }

    fun getInterestChip(){
        var result = StringBuilder("")
        for (i in 0 until interest_chip_group.childCount) {
            val chip = interest_chip_group.getChildAt(i) as Chip
            // if (chip.isChecked) {
            result.append(chip.text).append(" ")
            //  }
        }

        edit.putString("user_interests", result.toString())
        edit.commit()
        Toast.makeText(context, result.toString(), Toast.LENGTH_LONG).show()
    }

    fun getSkillChip(){
        var result = StringBuilder("")
        for (i in 0 until skill_chip_group.childCount) {
            val chip = skill_chip_group.getChildAt(i) as Chip
           // if (chip.isChecked) {
                result.append(chip.text).append(" ")
          //  }
        }

        edit.putString("user_skills", result.toString())
        edit.commit()
        Toast.makeText(context, result.toString(), Toast.LENGTH_LONG).show()
    }

    fun postDefaultInterest() {
        val default_interest =
            "Science Engineering AI Law".split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        val inflater = LayoutInflater.from(context)
        for (text in default_interest) {
            val chip_item = inflater.inflate(R.layout.interests_chip_item, null, false) as Chip
            chip_item.text = text
//                chip_item.setOnClickListener {view->
//                    chip_group.removeView(view)
//                }
            interest_chip_group.addView(chip_item)
        }
    }

    override fun onOffsetChanged(p0: AppBarLayout?, verticalOffset: Int) {


        if (scrollRange == -1) {
            scrollRange = appBarLayout.totalScrollRange
        }
        if (scrollRange + verticalOffset == 0) {
            collaspingToolbar.title = "Companies"
            isShow = true
            toolbar.visibility = View.VISIBLE

            setHasOptionsMenu(true)
        } else if (isShow) {
            collaspingToolbar.title = ""
            isShow = false
            setHasOptionsMenu(false)
            toolbar.visibility = View.GONE
        }
    }

//    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
//
//        super.onCreateOptionsMenu(menu, inflater)
//
//
//
//        inflater!!.inflate(R.menu.menu_company, menu)
//        val menuItem: MenuItem = menu!!.findItem(R.id.searchMenu)
//
//        val searchView = menuItem.actionView as SearchView
//        searchView.imeOptions = EditorInfo.IME_ACTION_DONE
//        // searchView.setBackgroundDrawable(resources.getDrawable(R.drawable.round_edittext_sec))
//        // searchView.setBackgroundColor(Color.parseColor("#ffffff"))
//        //materialSearchView.setMenuItem(menuItem)
//    }

    companion object {

        @JvmStatic
        fun newInstance() =
            ProfileFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}
