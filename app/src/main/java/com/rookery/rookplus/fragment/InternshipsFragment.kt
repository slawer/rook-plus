package com.rookery.rookplus.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.rookery.rookplus.R


class InternshipsFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_internships, container, false)
        return rootView
    }



    companion object {


        @JvmStatic
        fun newInstance() =
            InternshipsFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}
