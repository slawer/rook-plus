package com.rookery.rookplus.fragment

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.miguelcatalan.materialsearchview.MaterialSearchView
import com.rookery.rookplus.R
import androidx.core.view.MenuItemCompat.getActionView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.kittinunf.fuel.Fuel
import com.rookery.rookplus.adapter.CompanyAdapter
import com.rookery.rookplus.model.CompanyModel
import com.rookery.rookplus.util.APILinks
import dmax.dialog.SpotsDialog
import org.json.JSONArray
import org.json.JSONObject
import java.util.*


class CompanyFragment : Fragment(), AppBarLayout.OnOffsetChangedListener {


    private lateinit var toolbar: Toolbar
    private lateinit var materialSearchView: MaterialSearchView
    private lateinit var collaspingToolbar: CollapsingToolbarLayout
    private lateinit var appBarLayout: AppBarLayout

    lateinit var companyArrayList: ArrayList<CompanyModel>
    lateinit var companyAdapter: CompanyAdapter
    lateinit var recyclerCompany: RecyclerView

    private lateinit var prefs: SharedPreferences
    private lateinit var edit: SharedPreferences.Editor

    private lateinit var dialog: SpotsDialog

    var isShow: Boolean = false
    var scrollRange = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }


    @SuppressLint("WrongConstant")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_company, container, false)

        dialog = SpotsDialog.Builder().setContext(context).build() as SpotsDialog


        collaspingToolbar = rootView.findViewById(R.id.collaspingToolbar)
        toolbar = collaspingToolbar.findViewById(R.id.toolbar1)
        appBarLayout = rootView.findViewById(R.id.appBarLayout)
        recyclerCompany = rootView.findViewById(R.id.recyclerCompany)

        toolbar.visibility = View.INVISIBLE
//        toolbar.title = "Hello"
//        toolbar.setTitleTextColor(Color.parseColor("#F44336"))
        toolbar.setNavigationIcon(R.drawable.mini_rook)

        prefs = PreferenceManager.getDefaultSharedPreferences(context)
        edit = prefs.edit()

     //  (activity as AppCompatActivity).supportActionBar!!.title = "jhgtyd"
       (activity as AppCompatActivity).setSupportActionBar(toolbar)
//        (activity as AppCompatActivity).supportActionBar!!.title = "Material"
//        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"))
//        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
       // (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(true)
   //     (activity as AppCompatActivity).supportActionBar!!.title = "Title"
//        if (toolbar != null) {
//            (activity as AppCompatActivity).setSupportActionBar(toolbar)
//
//            val actionBar = (activity as AppCompatActivity).supportActionBar
//            if (actionBar != null) {
//                actionBar.setDisplayHomeAsUpEnabled(true)
//                actionBar.setDisplayShowTitleEnabled(true)
//                actionBar.setTitle("Transfer Funds Via Mobile Wallet")
//                actionBar.setDisplayUseLogoEnabled(false)
//                actionBar.setHomeButtonEnabled(true)
//            }
//        }

        companyArrayList = java.util.ArrayList()

        companyAdapter = CompanyAdapter(companyArrayList, context)

        recyclerCompany.setHasFixedSize(true)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerCompany.layoutManager = llm
        //recyclerAssignedReq.itemAnimator = SlideInUpAnimator(OvershootInterpolator(1f))
        //recyclerAssignedReq.itemAnimator = DefaultItemAnimator()

        recyclerCompany.adapter = companyAdapter



        appBarLayout.addOnOffsetChangedListener(this)

        materialSearchView = rootView.findViewById(
            R.id.searchView
        )


        getCompanyList()

        return rootView
    }

    fun getCompanyList() {
        dialog.setMessage("Loading...")
        dialog.show()
        val timeout = 5000 // 5000 milliseconds = 5 seconds.
        val timeoutRead = 60000 // 60000 milliseconds = 1 minute.


        Fuel.get(APILinks.COMP_LIST_URL).header("x-ROOK-token" to prefs.getString("user_jwt","")).timeout(timeout).timeoutRead(timeoutRead).responseString { request, response, result ->
            //            println(request)
//            println(response)
            val (data, error) = result
            println(request.httpString())
            if (error == null) {
                println(data.toString())
                //do something when success
                val jO = JSONObject(data.toString())

                val jsonInner = JSONObject(jO.getString("result"))
                val pendingReq = JSONArray(jsonInner.getString("companies"))

                companyArrayList.clear()

                var assignedRequest: CompanyModel
                for (i in 0 until pendingReq.length()) {
                    val rnd = Random()
                    val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

                    val escalationObject = pendingReq.getJSONObject(i)
                    val cname = escalationObject.getString("cname")
                    val type = escalationObject.getString("type")
                    val location = escalationObject.getString("location")
                    val bio = escalationObject.getString("bio")
                    val logo = escalationObject.getString("logo")
                    val subscribed = escalationObject.getString("subscribed")


                   // val strBool = status.toString()
                  //  Log.e("strBool", strBool)


                        assignedRequest = CompanyModel(cname, type, location, bio, logo, subscribed)


                        companyArrayList.add(assignedRequest)

                }
                dialog.dismiss()
                companyAdapter.notifyDataSetChanged()
            } else {
                //error handling
            }


        }
    }

    override fun onOffsetChanged(p0: AppBarLayout?, verticalOffset: Int) {


        if (scrollRange == -1) {
            scrollRange = appBarLayout.totalScrollRange
        }
        if (scrollRange + verticalOffset == 0) {
            collaspingToolbar.title = "Companies"
            isShow = true
            toolbar.visibility = View.VISIBLE

            setHasOptionsMenu(true)
        } else if (isShow) {
            collaspingToolbar.title = ""
            isShow = false
            setHasOptionsMenu(false)
            toolbar.visibility = View.GONE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {

        super.onCreateOptionsMenu(menu, inflater)



        inflater!!.inflate(R.menu.menu_company, menu)
        val menuItem: MenuItem = menu!!.findItem(R.id.searchMenu)

        val searchView = menuItem.actionView as SearchView
        searchView.imeOptions = EditorInfo.IME_ACTION_DONE
       // searchView.setBackgroundDrawable(resources.getDrawable(R.drawable.round_edittext_sec))
       // searchView.setBackgroundColor(Color.parseColor("#ffffff"))
        //materialSearchView.setMenuItem(menuItem)
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            CompanyFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}
