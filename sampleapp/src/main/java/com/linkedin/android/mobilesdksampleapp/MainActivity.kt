/*
    Copyright 2014 LinkedIn Corp.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.linkedin.android.mobilesdksampleapp

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.pm.Signature
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

import com.linkedin.platform.LISession
import com.linkedin.platform.errors.LIAuthError
import com.linkedin.platform.errors.LIDeepLinkError
import com.linkedin.platform.listeners.AuthListener
import com.linkedin.platform.LISessionManager
import com.linkedin.platform.utils.Scope

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val thisActivity = this
        setUpdateState()

        //Initialize session
        val liLoginButton = findViewById<View>(R.id.login_li_button) as Button
        liLoginButton.setOnClickListener {
            LISessionManager.getInstance(applicationContext).init(thisActivity, buildScope(), object : AuthListener {
                override fun onAuthSuccess() {
                    setUpdateState()
                    Toast.makeText(
                        applicationContext,
                        "success" + LISessionManager.getInstance(applicationContext).session.accessToken.toString(),
                        Toast.LENGTH_LONG
                    ).show()
                }

                override fun onAuthError(error: LIAuthError) {
                    setUpdateState()
                    (findViewById<View>(R.id.at) as TextView).text = error.toString()
                    Toast.makeText(applicationContext, "failed $error", Toast.LENGTH_LONG).show()
                }
            }, true)
        }

        //Clear session
        val liForgetButton = findViewById<View>(R.id.logout_li_button) as Button
        liForgetButton.setOnClickListener {
            LISessionManager.getInstance(applicationContext).clearSession()
            setUpdateState()
        }

        //Go to API calls page
        val liApiCallButton = findViewById<View>(R.id.apiCall) as Button
        liApiCallButton.setOnClickListener {
            val intent = Intent(this@MainActivity, ApiActivity::class.java)
            startActivity(intent)
        }

        //Go to DeepLink page
        val liDeepLinkButton = findViewById<View>(R.id.deeplink) as Button
        liDeepLinkButton.setOnClickListener {
            val intent = Intent(this@MainActivity, DeepLinkActivity::class.java)
            startActivity(intent)
        }


        //Compute application package and hash
        val liShowPckHashButton = findViewById<View>(R.id.showPckHash) as Button
        liShowPckHashButton.setOnClickListener {
            try {
                val info = packageManager.getPackageInfo(
                    PACKAGE_MOBILE_SDK_SAMPLE_APP,
                    PackageManager.GET_SIGNATURES
                )
                for (signature in info.signatures) {
                    val md = MessageDigest.getInstance("SHA")
                    md.update(signature.toByteArray())

                    (findViewById<View>(R.id.pckText) as TextView).text = info.packageName
                    (findViewById<View>(R.id.pckHashText) as TextView).text =
                        Base64.encodeToString(md.digest(), Base64.NO_WRAP)
                }
            } catch (e: PackageManager.NameNotFoundException) {
                Log.d(TAG, e.message, e)
            } catch (e: NoSuchAlgorithmException) {
                Log.d(TAG, e.message, e)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        LISessionManager.getInstance(applicationContext).onActivityResult(this, requestCode, resultCode, data)
    }

    private fun setUpdateState() {
        val sessionManager = LISessionManager.getInstance(applicationContext)
        val session = sessionManager.session
        val accessTokenValid = session.isValid

        (findViewById<View>(R.id.at) as TextView).text =
            if (accessTokenValid) session.accessToken.toString() else "Sync with LinkedIn to enable these buttons"
        (findViewById<View>(R.id.apiCall) as Button).isEnabled = accessTokenValid
        (findViewById<View>(R.id.deeplink) as Button).isEnabled = accessTokenValid
    }

    companion object {
        private val TAG = MainActivity::class.java!!.getSimpleName()
        val PACKAGE_MOBILE_SDK_SAMPLE_APP = "com.linkedin.android.mobilesdksampleapp"

        private fun buildScope(): Scope {
            return Scope.build(Scope.R_BASICPROFILE, Scope.W_SHARE)
        }
    }

}
