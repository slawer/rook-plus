/*
    Copyright 2014 LinkedIn Corp.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.linkedin.android.mobilesdksampleapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

import com.linkedin.platform.errors.LIDeepLinkError
import com.linkedin.platform.listeners.DeepLinkListener
import com.linkedin.platform.DeepLinkHelper


class DeepLinkActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deeplink)

        //Opens other LinkedIn member's profile.
        val liOpenProfileButton = findViewById<View>(R.id.deeplink_open_profile) as Button
        liOpenProfileButton.setOnClickListener {
            val profileId = (findViewById<View>(R.id.deeplink_profile_id) as EditText).text.toString()

            val deepLinkHelper = DeepLinkHelper.getInstance()
            deepLinkHelper.openOtherProfile(this@DeepLinkActivity, profileId, object : DeepLinkListener {
                override fun onDeepLinkSuccess() {
                    (findViewById<View>(R.id.deeplink_error) as TextView).text = ""
                }

                override fun onDeepLinkError(error: LIDeepLinkError) {
                    (findViewById<View>(R.id.deeplink_error) as TextView).text = error.toString()
                }
            })
        }

        //Opens current logged in member's profile
        val liMyProfileButton = findViewById<View>(R.id.myProfile) as Button
        liMyProfileButton.setOnClickListener {
            val deepLinkHelper = DeepLinkHelper.getInstance()
            deepLinkHelper.openCurrentProfile(this@DeepLinkActivity, object : DeepLinkListener {
                override fun onDeepLinkSuccess() {
                    (findViewById<View>(R.id.deeplink_error) as TextView).text = ""
                }

                override fun onDeepLinkError(error: LIDeepLinkError) {
                    (findViewById<View>(R.id.deeplink_error) as TextView).text = error.toString()
                }
            })
        }

    }

    //To get call back
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        val deepLinkHelper = DeepLinkHelper.getInstance()
        deepLinkHelper.onActivityResult(this, requestCode, resultCode, data)
    }

}
