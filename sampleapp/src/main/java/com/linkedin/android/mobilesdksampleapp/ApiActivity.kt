/*
    Copyright 2014 LinkedIn Corp.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.linkedin.android.mobilesdksampleapp

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

import com.linkedin.platform.errors.LIApiError
import com.linkedin.platform.listeners.ApiResponse
import com.linkedin.platform.listeners.ApiListener
import com.linkedin.platform.APIHelper


class ApiActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_api)
        val makeApiCall = findViewById<View>(R.id.makeApiCall) as Button
        makeApiCall.setOnClickListener {
            val apiHelper = APIHelper.getInstance(applicationContext)
            apiHelper.getRequest(this@ApiActivity, topCardUrl, object : ApiListener {
                override fun onApiSuccess(s: ApiResponse) {
                    (findViewById<View>(R.id.response) as TextView).text = s.toString()
                }

                override fun onApiError(error: LIApiError) {
                    (findViewById<View>(R.id.response) as TextView).text = error.toString()
                }
            })
        }

        val makePostApiCall = findViewById<View>(R.id.makePostApiCall) as Button
        makePostApiCall.setOnClickListener {
            val shareComment = findViewById<View>(R.id.shareComment) as EditText
            val shareJsonText = "{ \n" +
                    "   \"comment\":\"" + shareComment.text + "\"," +
                    "   \"visibility\":{ " +
                    "      \"code\":\"anyone\"" +
                    "   }," +
                    "   \"content\":{ " +
                    "      \"title\":\"Test Share Title\"," +
                    "      \"description\":\"Leverage the Share API to maximize engagement on user-generated content on LinkedIn\"," +
                    "      \"submitted-url\":\"https://www.americanexpress.com/us/small-business/openforum/programhub/managing-your-money/?pillar=critical-numbers\"," +
                    "      \"submitted-image-url\":\"http://m3.licdn.com/media/p/3/000/124/1a6/089a29a.png\"" +
                    "   }" +
                    "}"
            val apiHelper = APIHelper.getInstance(applicationContext)
            apiHelper.postRequest(this@ApiActivity, shareUrl, shareJsonText, object : ApiListener {
                override fun onApiSuccess(apiResponse: ApiResponse) {
                    (findViewById<View>(R.id.response) as TextView).text = apiResponse.toString()
                }

                override fun onApiError(error: LIApiError) {
                    (findViewById<View>(R.id.response) as TextView).text = error.toString()
                }
            })
        }

    }

    companion object {

        private val host = "api.linkedin.com"
        private val topCardUrl = "https://$host/v1/people/~:(first-name,last-name,public-profile-url)"
        private val shareUrl = "https://$host/v1/people/~/shares"
    }

}
